<?php include('header.php'); ?>
<div id="tg-homeslider" class="tg-homeslider tg-haslayout">
			<figure class="item">
				<img src="images/slider/img-01.jpg" alt="image description">
				<figcaption>
					<div class="container">
						<div class="row">
							<div class="col-md-5 col-sm-10 col-xs-12 pull-right nopadding">
						<h1 style="text-align: left;"><span style="font-size: xx-large;"><span>กรอกรายละเอียดเพื่อรับข้อมูลเพิ่มเติม<br /></span></span></h1></br>
								<img class="tg-svginject" src="images/img-01.svg" alt="image description">

								<div class="tg-description nopadding">
								
												<div class="tg-widgetcontent">
													<form class="tg-themeform">
														<fieldset>
															<div class="form-group">
																<input type="text" class="form-control" name="" placeholder="ชื่อ-นามสกุล">
															</div>
															<div class="form-group">
																<input type="text" class="form-control" name="" placeholder="วัน เดือน ปี เกิด (พ.ศ.)	">
															</div>
															<div class="form-group">
																<input type="text" class="form-control" name="" placeholder="เบอร์โทรศัพท์">
															</div>
															<div class="form-group">
																<input type="text" class="form-control" name="" placeholder="อีเมล">
															</div>
															<div class="form-group">
																<div class="tg-select">
																	<select>
																		<option>I would like to discuss</option>
																		<option>I would like to discuss</option>
																		<option>I would like to discuss</option>
																	</select>
																</div>
															</div>
															<button class="tg-btn" type="submit"><span>i’m waiting</span></button>
														</fieldset>
													</form>
												</div>
										
								
								</div>

						</div>
					</div>					</div>
				</figcaption>
			</figure>
			<figure class="item">
				<img src="images/slider/img-03.jpg" alt="image description">
				<figcaption>
					<div class="container">
						<div class="row">
<div class="col-md-7 col-sm-10 col-xs-12 pull-right" style="padding: 30px 50px 100px 50px;">
							
						<h1 style="text-align: left;"><span style="font-size: xx-large; letter-spacing: 1.5px;
    line-height: 26px;"><span>ไอชายด์ เฮลท์ โพรเทคชั่น<br /></span></span></h1></br>
								<img class="tg-svginject" src="images/img-01.svg" alt="image description">
										<p style="text-align: left;"><span style="font-size: 16px; letter-spacing: 1.5px;
    line-height: 26px;"><span>อีกระดับของความคุ้มครองสุขภาพเด็กจาก กรุงไทย-แอกซ่า ประกันชีวิต เพื่อให้เจ้าตัวน้อยของคุณ
								เรียนรู้ได้อย่างเต็มที่โดยไม่ต้องกังวลอีกต่อไป ด้วยความคุ้มครอง<strong>ประกันสุขภาพเด็ก</strong> ทั้งผู้ป่วยนอก และผู้ป่วยใน<strong> ไอชายด์ เฮลท์ โพรเทคชั่น</strong></span></span></p>


								<div class="tg-description">
								<div class="tg-btnsbox">
								<div class="sppb-addon-content" style="margin:20px;letter-spacing: 1.5px;
    line-height: 26px;">
									<a class="tg-btn" href="#"><span>i-Child Health Protection</span></a>
									<a class="tg-btn" href="#"><span>ให้เราติดต่อกลับ</span></a>
								</div>
								
							</div>
						</div>
					</div>
				</div>	</div></figcaption>
			</figure>
			<figure class="item">
				<img src="images/slider/img-02.jpg" alt="image description">
				<figcaption>
					<div class="container">
						<div class="row">
<div class="col-md-6 col-sm-10 col-xs-12 pull-right" style="
    padding-left: 50px
">
						<h1 style="text-align: left;"><span style="font-size: xx-large; letter-spacing: 1.5px;
    line-height: 26px;"><span>สุขภาพดี 4200 6200<br></span></span></h1><br>
								<svg xmlns="http://www.w3.org/2000/svg" width="165.21" height="27.907" viewBox="0 0 165.21 27.907" class="tg-svginject  replaced-svg" data-url="images/img-01.svg">
  <defs>
    <style>
      .cls-1 {
        fill: #004281;
        fill-rule: evenodd;
      }
    </style>
  </defs>
  <path id="Line" class="cls-1" d="M1375.46,607.177a254.459,254.459,0,0,1,117.58-24.223,237.571,237.571,0,0,1,32.92,3.471c0.76,0.136,4.88,1.09,5.15-.2,0.26-1.237-4.1-2.16-4.63-2.257-42.19-7.541-85.94-5.264-126.89,7.543a259.972,259.972,0,0,0-33.43,13.035c-1.38.656,2.86,2.163,3.1,2.23,1.74,0.5,4.44,1.236,6.2.4h0Z" transform="translate(-1365.88 -579.656)"></path>
</svg>

								<div class="tg-description nopadding">
									<p style="text-align: left; letter-spacing: 1.5px;
    line-height: 26px;"><span style="font-size: 14px;"><span><strong>ประกันสุขภาพเด็ก</strong> และผู้ใหญ่ แผนสุขภาพดี 4200 และ 6200 คุ้มค่ากับแผนความคุ้มครองเงื่อนไขพิเศษ ไม่บังคับซื้อสัญญาหลักที่แพงเกินไป</span></span></p>
<div style="text-align: left;"><span style="font-size: 13px;">
</span>
<p><span style="color: #ff0000; text-align: center; letter-spacing: 1.5px;
    line-height: 26px;">แผนประกันนี้จำหน่ายถึง 30 ธันวาคม 2559 นี้เท่านั้น</span></p
    >
</div>

								</div>
								<div class="tg-btnsbox">
								<div class="sppb-addon-content" style="margin:-0px;letter-spacing: 1.5px;
    line-height: 26px;">
									<a class="tg-btn" href="#"><span>สุขภาพเด็ก 4200 6200</span></a>
									<a class="tg-btn" href="#"><span>ให้เราติดต่อกลับ</span></a>
								</div>
							</div>
						</div>
						
						
						</div>
					</div>
				</figcaption>
			</figure>
		</div>
		
<section class="tg-main-section tg-paddingbottomzero tg-haslayout">
				<div class="container">
					<div class="row">
						<div class="col-lg-offset-2 col-lg-8 col-md-offset-1 col-md-10 col-sm-offset-0 col-sm-12 col-xs-12">
							<div class="tg-sectionhead">
								<div class="tg-sectiontitle">
									<h2>กรุงไทย-แอกซ่า ประกันชีวิต</h2>
										<h3 style="text-align: center;">แผนประกันชีวิตที่ครบทุกความคุ้มครอง</h3>
								</div>
								<div class="tg-description">
									<p>กรุงไทย-แอกซ่า ประกันชีวิต
 อยู่เคียงข้างคุณในทุกก้าวย่างของชีวิต ตั้งแต่จบการศึกษา แต่งงาน มีลูก ไปจนถึงการวางแผนเกษียณอายุ 
โดยมอบแบบประกันที่คลอบคลุมทุกความต้องการที่จะช่วยดูแลคนที่คุณรัก เราพร้อมดูแลคุณทั้งชีวิต</p>
								</div>
							</div>
						</div>
						<div class="col-md-8 col-sm-12 col-xs-12">
							<div class="row">
								<div class="tg-services tg-servicestyletwo">
									<div class="col-sm-6">
										<div class="tg-box">
											<div class="tg-service">
												<span class="tg-seviceicon">
													<i class="fa fa-heartbeat"></i>
												</span>
												<div class="tg-heading">
													<h4><a href="#">ประกันสุขภาพ</a></h4>
												</div>
												<div class="tg-description">
													<p>ที่สุดของแผนประกันสุขภาพ คุ้มครอง ครอบคลุมทุกความต้องการ ทุกที่ ทุกเวลา</p>
							
												</div>
											</div>
										</div>
									</div>
									<div class="col-sm-6">
										<div class="tg-box">
											<div class="tg-service">
												<span class="tg-seviceicon">
													<i class="fa fa-shield"></i>
												</span>
												<div class="tg-heading">
													<h4><a href="#">ประกันคุ้มครองรายได้</a></h4>
												</div>
												<div class="tg-description">
													<p>ใครจะดูแล“คนข้างหลัง”ของคุณได้ดีเท่ากับสี่งที่คุณเตรียมไว้ให้พวกเขาเอง</p>
								</div>
											</div>
										</div>
									</div>
									<div class="col-sm-6">
										<div class="tg-box">
											<div class="tg-service">
												<span class="tg-seviceicon">
													<i class="fa fa-line-chart"></i>
												</span>
												<div class="tg-heading">
													<h4><a href="#">ประกันสะสมทรัพย์</a></h4>
												</div>
												<div class="tg-description">
														<p>ไม่มีคำว่าเร็วเกินไปสำหรับการสร้างรากฐานทางการเงินที่มั่นคง
</p>
								</div>
											</div>
										</div>
									</div>
									<div class="col-sm-6">
										<div class="tg-box">
											<div class="tg-service">
												<span class="tg-seviceicon">
													<i class="fa fa-balance-scale"></i>
												</span>
												<div class="tg-heading">
													<h4><a href="#">ประกันชีวิตแบบบำนาญ</a></h4>
												</div>
												<div class="tg-description">
												<p>เพราะการเกษียณไม่ใช่จุดสิ้นสุดของชีวิต แต่เป็นจุดเริ่มต้นของการใช้ชีวิต</p>
									</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-sm-4 hidden-sm hidden-xs">
							<figure class="tg-serviceimg">
								<img src="images/img-03.jpg" alt="image description">
							</figure>
						</div>
					</div>
				</div>
			</section>

	<section class="tg-bglight tg-haslayout">
				<div class="col-sm-6">
					<div class="row">
						<div class="tg-videosection">
							<figure>
								<img src="images/img-01.jpg" alt="image description">
								<figcaption>
									<a class="tg-btnplay" href="#"><i class="fa fa-play"></i></a>
									<h2>"เพราะการปกป้องที่ดีที่สุด คือการปล่อยให้เค้าเรียนรู้"</h2>
								</figcaption>
							</figure>
						</div>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="tg-whychooseus">
						<div class="tg-sectionhead">
							<div class="tg-sectiontitle">
								<h2>แผนประกันที่เหมาะสมและตรงกับความต้องการของคุณ</h2>
										<h3 style="text-align: center;">กรุงไทย-แอกซ่า ประกันชีวิต</h3>
							</div>
							<div class="tg-description">
								<p>อยู่เคียงข้างคุณในทุกก้าวย่างของชีวิต ตั้งแต่จบการศึกษา แต่งงาน มีลูก ไปจนถึงการวางแผนเกษียณอายุ โดยมอบแบบประกัน
								ที่คลอบคลุมทุกความต้องการที่จะช่วยดูแลคนที่คุณรัก เราพร้อมดูแลคุณทั้งชีวิต</p>
								
						</br>
								<h4>- Reliable ไว้ใจได้</h4></br>
										<h5>เราทำในสิ่งที่พูด และเราพูดในสิ่งที่เราจะทำ คุณจึงไว้ใจเราได้</h5></br>
									<h4>- Available พร้อมให้บริการ</h4>
								</br><h5>เรารับฟังลูกค้าอย่างแท้จริงและเราพร้อมที่จะให้บริการทุกที่ทุกเวลาที่ลูกค้าต้องการ
</h5>	</br>	
<h4>- Attentive ใส่ใจลูกค้าเสมอ</h4>
								</br>
								<h5>เราปฏิบัติต่อลูกค้าด้วยความเข้าใจและเอาใจใส่ต่อความต้องการเฉพาะบุคคล</h5>
							
							</div>
						</div>
				
					</div>
				</div>
			</section>
	

			
	<!--
				<div class="row">
						<div class="col-lg-offset-2 col-lg-8 col-md-offset-1 col-md-10 col-sm-offset-0 col-sm-12 col-xs-12">
							<div class="tg-sectionhead">
								<div class="tg-sectiontitle">
									<h2>กรุงไทย-แอกซ่า ประกันชีวิต</h2>
										<h3 style="text-align: center;">แบบประกัน</h3>
								</div>
								<div class="tg-description">
									<p>เรานำเสนอแผนประกันที่เหมาะสมและตรงกับความต้องการของคุณ ให้คุณสบายใจ 
									ไร้กังวล เมื่อเกิดเหตุการณ์ไม่คาดฝัน นอกจากนี้ เรายังมุ่งมั่นช่วยให้คุณบรรลุเป้าหมายทางการเงินที่ตั้งไว้ 
									และทำความฝันให้เป็นจริงได้อย่างง่ายดาย ด้วยแผนความคุ้มครองทางการเงิน 
									แผนการลงทุน และแผนเพื่อการเกษียณอายุที่เหมาะสมและหลากหลาย</p>
								</div>
							</div>
						</div>
						<div class="tg-themetabs">
							<nav class="tg-themetabnav">
								<ul role="tablist">
									<li role="presentation" class="active"><a href="#all" aria-controls="all" role="tab" data-toggle="tab">ทั้งหมด</a></li>
									<li role="presentation"><a href="#investment" aria-controls="investment" role="tab" data-toggle="tab">ประกันสุขภาพ</a></li>
									<li role="presentation"><a href="#taxes" aria-controls="taxes" role="tab" data-toggle="tab">ประกันคุ้มครองรายได้</a></li>
									<li role="presentation"><a href="#planning" aria-controls="planning" role="tab" data-toggle="tab">ประกันสะสมทรัพย์</a></li>
									<li role="presentation"><a href="#financial" aria-controls="financial" role="tab" data-toggle="tab">ประกันชีวิตแบบบำนาญ</a></li>
								</ul>
							</nav>
							<div class="tab-content tg-themetabcontent">
								<div role="tabpanel" class="tab-pane in active" id="all">
									<div id="tg-projectsliderall" class="tg-projectsliderall tg-projects">
										<div class="item tg-project">
											<figure>
												<img src="images/projects/img-01.jpg" alt="image description">
												<figcaption>
													<h4><a href="#">sicing elit sed doi tempor</a></h4>
												</figcaption>
											</figure>
										</div>
										<div class="item tg-project">
											<figure>
												<img src="images/projects/img-02.jpg" alt="image description">
												<figcaption>
													<h4><a href="#">sicing elit sed doi tempor</a></h4>
												</figcaption>
											</figure>
										</div>
										<div class="item tg-project">
											<figure>
												<img src="images/projects/img-03.jpg" alt="image description">
												<figcaption>
													<h4><a href="#">sicing elit sed doi tempor</a></h4>
												</figcaption>
											</figure>
										</div>
										<div class="item tg-project">
											<figure>
												<img src="images/projects/img-04.jpg" alt="image description">
												<figcaption>
													<h4><a href="#">sicing elit sed doi tempor</a></h4>
												</figcaption>
											</figure>
										</div>
									
										<div class="item tg-project">
											<figure>
												<img src="images/projects/img-01.jpg" alt="image description">
												<figcaption>
													<h4><a href="#">sicing elit sed doi tempor</a></h4>
												</figcaption>
											</figure>
										</div>
										<div class="item tg-project">
											<figure>
												<img src="images/projects/img-02.jpg" alt="image description">
												<figcaption>
													<h4><a href="#">sicing elit sed doi tempor</a></h4>
												</figcaption>
											</figure>
										</div>
										<div class="item tg-project">
										<figure>
												<img src="images/projects/img-02.jpg" alt="image description">
												<figcaption>
													<h4><a href="#">เพอร์เฟคเฮลท์ โซลูชั่น</a></h4>
												</figcaption>
											</figure>
										</div>
										<div class="item tg-project">
											<figure>
												<img src="images/projects/img-04.jpg" alt="image description">
												<figcaption>
													<h4><a href="#">sicing elit sed doi tempor</a></h4>
												</figcaption>
											</figure>
										</div>
										<div class="item tg-project">
											<figure>
												<img src="images/projects/img-05.jpg" alt="image description">
												<figcaption>
													<h4><a href="#">sicing elit sed doi tempor</a></h4>
												</figcaption>
											</figure>
										</div>
										<div class="item tg-project">
											<figure>
												<img src="images/projects/img-01.jpg" alt="image description">
												<figcaption>
													<h4><a href="#">sicing elit sed doi tempor</a></h4>
												</figcaption>
											</figure>
										</div>
										<div class="item tg-project">
											<figure>
												<img src="images/projects/img-02.jpg" alt="image description">
												<figcaption>
													<h4><a href="#">sicing elit sed doi tempor</a></h4>
												</figcaption>
											</figure>
										</div>
										<div class="item tg-project">
											<figure>
												<img src="images/projects/img-03.jpg" alt="image description">
												<figcaption>
													<h4><a href="#">sicing elit sed doi tempor</a></h4>
												</figcaption>
											</figure>
										</div>
										<div class="item tg-project">
											<figure>
												<img src="images/projects/img-04.jpg" alt="image description">
												<figcaption>
													<h4><a href="#">sicing elit sed doi tempor</a></h4>
												</figcaption>
											</figure>
										</div>
										<div class="item tg-project">
											<figure>
												<img src="images/projects/img-05.jpg" alt="image description">
												<figcaption>
													<h4><a href="#">sicing elit sed doi tempor</a></h4>
												</figcaption>
											</figure>
										</div>
									</div>
								</div>
								<div role="tabpanel" class="tab-pane" id="investment">
									<div id="tg-projectsliderinvestment" class="tg-projectsliderinvestment tg-projects">
										<div class="item tg-project">
											<figure>
												<img src="images/projects/img-01.jpg" alt="image description">
												<figcaption>
													<h4><a href="#">ไอชายด์ เฮลท์ โพรเทคชั่น</a></h4>
												</figcaption>
											</figure>
										</div>
										<div class="item tg-project">
											<figure>
												<img src="images/projects/img-02.jpg" alt="image description">
												<figcaption>
													<h4><a href="#">เพอร์เฟคเฮลท์ โซลูชั่น</a></h4>
												</figcaption>
											</figure>
										</div>
										<div class="item tg-project">
											<figure>
												<img src="images/projects/img-03.jpg" alt="image description">
												<figcaption>
													<h4><a href="#">ประกันสุขภาพเด็ก อายุ 0-15 ปี</a></h4>
												</figcaption>
											</figure>
										</div>
										<div class="item tg-project">
											<figure>
												<img src="images/projects/img-04.jpg" alt="image description">
												<figcaption>
													<h4><a href="#">ประกันสุขภาพ สุขภาพดี 6200</a></h4>
												</figcaption>
											</figure>
										</div>
										<div class="item tg-project">
											<figure>
												<img src="images/projects/img-01.jpg" alt="image description">
												<figcaption>
													<h4><a href="#">ประกันสุขภาพ วัยทำงาน</a></h4>
												</figcaption>
											</figure>
										</div>
										<div class="item tg-project">
											<figure>
												<img src="images/projects/img-02.jpg" alt="image description">
												<figcaption>
													<h4><a href="#">ประกันสุขภาพ แบบออม 12 ปี</a></h4>
												</figcaption>
											</figure>
										</div>
										<div class="item tg-project">
											<figure>
												<img src="images/projects/img-03.jpg" alt="image description">
												<figcaption>
													<h4><a href="#">ประกันสุขภาพ แบบออม 20 ปี</a></h4>
												</figcaption>
											</figure>
										</div>
										<div class="item tg-project">
											<figure>
												<img src="images/projects/img-05.jpg" alt="image description">
												<figcaption>
													<h4><a href="#">ประกันโรคร้ายแรง i-Care</a></h4>
												</figcaption>
											</figure>
										</div>
									</div>
								</div>
								<div role="tabpanel" class="tab-pane" id="taxes">
									<div id="tg-projectslidertaxes" class="tg-projectslidertaxes tg-projects">
										<div class="item tg-project">
											<figure>
												<img src="images/projects/img-04.jpg" alt="image description">
												<figcaption>
													<h4><a href="#">sicing elit sed doi tempor</a></h4>
												</figcaption>
											</figure>
										</div>
										<div class="item tg-project">
											<figure>
												<img src="images/projects/img-05.jpg" alt="image description">
												<figcaption>
													<h4><a href="#">sicing elit sed doi tempor</a></h4>
												</figcaption>
											</figure>
										</div>
										<div class="item tg-project">
											<figure>
												<img src="images/projects/img-01.jpg" alt="image description">
												<figcaption>
													<h4><a href="#">sicing elit sed doi tempor</a></h4>
												</figcaption>
											</figure>
										</div>
										<div class="item tg-project">
											<figure>
												<img src="images/projects/img-02.jpg" alt="image description">
												<figcaption>
													<h4><a href="#">sicing elit sed doi tempor</a></h4>
												</figcaption>
											</figure>
										</div>
										<div class="item tg-project">
											<figure>
												<img src="images/projects/img-03.jpg" alt="image description">
												<figcaption>
													<h4><a href="#">sicing elit sed doi tempor</a></h4>
												</figcaption>
											</figure>
										</div>
										<div class="item tg-project">
											<figure>
												<img src="images/projects/img-04.jpg" alt="image description">
												<figcaption>
													<h4><a href="#">sicing elit sed doi tempor</a></h4>
												</figcaption>
											</figure>
										</div>
										<div class="item tg-project">
											<figure>
												<img src="images/projects/img-05.jpg" alt="image description">
												<figcaption>
													<h4><a href="#">sicing elit sed doi tempor</a></h4>
												</figcaption>
											</figure>
										</div>
										<div class="item tg-project">
											<figure>
												<img src="images/projects/img-01.jpg" alt="image description">
												<figcaption>
													<h4><a href="#">sicing elit sed doi tempor</a></h4>
												</figcaption>
											</figure>
										</div>
										<div class="item tg-project">
											<figure>
												<img src="images/projects/img-02.jpg" alt="image description">
												<figcaption>
													<h4><a href="#">sicing elit sed doi tempor</a></h4>
												</figcaption>
											</figure>
										</div>
										<div class="item tg-project">
											<figure>
												<img src="images/projects/img-03.jpg" alt="image description">
												<figcaption>
													<h4><a href="#">sicing elit sed doi tempor</a></h4>
												</figcaption>
											</figure>
										</div>
									</div>
								</div>
								<div role="tabpanel" class="tab-pane" id="planning">
									<div id="tg-projectsliderplanning" class="tg-projectsliderplanning tg-projects">
										<div class="item tg-project">
											<figure>
												<img src="images/projects/img-01.jpg" alt="image description">
												<figcaption>
													<h4><a href="#">ประกันทุนการศึกษาบุตร iGrow</a></h4>
												</figcaption>
											</figure>
										</div>
										<div class="item tg-project">
											<figure>
												<img src="images/projects/img-02.jpg" alt="image description">
												<figcaption>
													<h4><a href="#">ประกันออมทรัพย์ iPlus</a></h4>
												</figcaption>
											</figure>
										</div>
										<div class="item tg-project">
											<figure>
												<img src="images/projects/img-04.jpg" alt="image description">
												<figcaption>
													<h4><a href="#">ประกันออมทรัพย์ 20SS</a></h4>
												</figcaption>
											</figure>
										</div>
										<div class="item tg-project">
											<figure>
												<img src="images/projects/img-05.jpg" alt="image description">
												<figcaption>
													<h4><a href="#">ประกันออมทรัพย์ 25PG</a></h4>
												</figcaption>
											</figure>
										</div>
								
									</div>
								</div>
								<div role="tabpanel" class="tab-pane" id="financial">
									<div id="tg-projectsliderfinancial" class="tg-projectsliderfinancial tg-projects">
										<div class="item tg-project">
											<figure>
												<img src="images/projects/img-01.jpg" alt="image description">
												<figcaption>
													<h4><a href="#">ประกันบำนาญ i-Wish 10</a></h4>
												</figcaption>
											</figure>
										</div>
										<div class="item tg-project">
											<figure>
												<img src="images/projects/img-02.jpg" alt="image description">
												<figcaption>
													<h4><a href="#">ประกันบำนาญ i-Wish 55</a></h4>
												</figcaption>
											</figure>
										</div>
										<div class="item tg-project">
											<figure>
												<img src="images/projects/img-03.jpg" alt="image description">
												<figcaption>
													<h4><a href="#">ประกันบำนาญ i-Wish 60</a></h4>
												</figcaption>
											</figure>
										</div>
										<div class="item tg-project">
											<figure>
												<img src="images/projects/img-04.jpg" alt="image description">
												<figcaption>
													<h4><a href="#">ประกันบำนาญ i-Wish 65</a></h4>
												</figcaption>
											</figure>
										</div>
							
									</div>
								</div>
							</div>
						</div>
					</div>
			<section class="tg-main-section tg-haslayout">
				<div class="container">
					<div class="row">
						<div class="col-lg-offset-2 col-lg-8 col-md-offset-1 col-md-10 col-sm-offset-0 col-sm-12 col-xs-12">
							<div class="tg-sectionhead">
								<div class="tg-sectiontitle">
									<h2>ตัวแทน กรุงไทย-แอกซ่า ประกันชีวิต</h2>
																<h3 style="text-align: center;">23094</h3>

								</div>
								<div class="tg-description">
									<p>เราเป็นตัวแทนที่มีประสบการณ์ ให้คำปรึกษากับลูกค้ามามากกว่า 20,000 คน ขอขอบคุณลูกค้าทุกท่านที่ให้ความไว้วางใจ เราให้คำมั่นว่าจะดูแลและบริการหลังการขายอย่างดีที่สุด</p>
								</div>
							</div>
						</div>
						<div id="tg-testimonialsslider" class="tg-testimonialsslider tg-clientfeedback">
							<div class="item tg-testimonial">
								<blockquote>
									<q>Tempor incididunt ut labore et dolore magna aliqua enim adiat minim sitaie veniam quis nostrud exercitation ullamco laboris nisi ut aliquip.</q>
								</blockquote>
								<div class="tg-clientinfo">
									<figure><a href="#"><img src="images/thumbs/img-01.jpg" alt="image description"></a></figure>
									<div class="tg-namecountery">
										<h4><a href="#">Christi jenny</a></h4>
										<span>Manchester</span>
									</div>
								</div>
							</div>
							<div class="item tg-testimonial">
								<blockquote>
									<q>Tempor incididunt ut labore et dolore magna aliqua enim adiat minim sitaie veniam quis nostrud exercitation ullamco laboris nisi ut aliquip.</q>
								</blockquote>
								<div class="tg-clientinfo">
									<figure><a href="#"><img src="images/thumbs/img-02.jpg" alt="image description"></a></figure>
									<div class="tg-namecountery">
										<h4><a href="#">Milford smith</a></h4>
										<span>Manchester</span>
									</div>
								</div>
							</div>
							<div class="item tg-testimonial">
								<blockquote>
									<q>Tempor incididunt ut labore et dolore magna aliqua enim adiat minim sitaie veniam quis nostrud exercitation ullamco laboris nisi ut aliquip.</q>
								</blockquote>
								<div class="tg-clientinfo">
									<figure><a href="#"><img src="images/thumbs/img-01.jpg" alt="image description"></a></figure>
									<div class="tg-namecountery">
										<h4><a href="#">Christi jenny</a></h4>
										<span>Manchester</span>
									</div>
								</div>
							</div>
							<div class="item tg-testimonial">
								<blockquote>
									<q>Tempor incididunt ut labore et dolore magna aliqua enim adiat minim sitaie veniam quis nostrud exercitation ullamco laboris nisi ut aliquip.</q>
								</blockquote>
								<div class="tg-clientinfo">
									<figure><a href="#"><img src="images/thumbs/img-02.jpg" alt="image description"></a></figure>
									<div class="tg-namecountery">
										<h4><a href="#">Milford smith</a></h4>
										<span>Manchester</span>
									</div>
								</div>
							</div>
							<div class="item tg-testimonial">
								<blockquote>
									<q>Tempor incididunt ut labore et dolore magna aliqua enim adiat minim sitaie veniam quis nostrud exercitation ullamco laboris nisi ut aliquip.</q>
								</blockquote>
								<div class="tg-clientinfo">
									<figure><a href="#"><img src="images/thumbs/img-01.jpg" alt="image description"></a></figure>
									<div class="tg-namecountery">
										<h4><a href="#">Christi jenny</a></h4>
										<span>Manchester</span>
									</div>
								</div>
							</div>
							<div class="item tg-testimonial">
								<blockquote>
									<q>Tempor incididunt ut labore et dolore magna aliqua enim adiat minim sitaie veniam quis nostrud exercitation ullamco laboris nisi ut aliquip.</q>
								</blockquote>
								<div class="tg-clientinfo">
									<figure><a href="#"><img src="images/thumbs/img-02.jpg" alt="image description"></a></figure>
									<div class="tg-namecountery">
										<h4><a href="#">Milford smith</a></h4>
										<span>Manchester</span>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section> -->
				
										
	<?php include('footerb.php'); ?>
