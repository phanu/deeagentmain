<?php include('header.php'); ?>
<div id="tg-innerbanner" class="tg-innerbanner tg-haslayout">
			<div class="tg-parallaximg" data-appear-top-offset="600" data-parallax="scroll" data-image-src="images/parallax/bgparallax-01.jpg"></div>
			<div class="container">
				<div class="row">
					<div class="col-sm-12">
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--************************************
				Inner Banner End
		*************************************-->
		<!--************************************
				Main Start
		*************************************-->
		<main id="tg-main" class="tg-main tg-haslayout">
			<!--************************************
					What We Offers Start
			*************************************-->
			<section class="tg-main-section tg-paddingbottomzero tg-haslayout">
				<div class="container">
					<div class="row">
						<div class="col-lg-offset-2 col-lg-8 col-md-offset-1 col-md-10 col-sm-offset-0 col-sm-12 col-xs-12">
							<div class="tg-sectionhead">
								<div class="tg-sectiontitle">
									<h2>เกี่ยวกัยเรา</h2>
										<h3 style="text-align: center;">แผนประกันชีวิตที่ครบทุกความคุ้มครอง</h3>
								</div>
								<div class="tg-description">
									<p>กรุงไทย-แอกซ่า ประกันชีวิต
 อยู่เคียงข้างคุณในทุกก้าวย่างของชีวิต ตั้งแต่จบการศึกษา แต่งงาน มีลูก ไปจนถึงการวางแผนเกษียณอายุ 
โดยมอบแบบประกันที่คลอบคลุมทุกความต้องการที่จะช่วยดูแลคนที่คุณรัก เราพร้อมดูแลคุณทั้งชีวิต</p>
								</div>
							</div>
						</div>
						<div class="col-md-8 col-sm-12 col-xs-12">
							<div class="row">
								<div class="tg-services tg-servicestyletwo">
									<div class="col-sm-6">
										<div class="tg-box">
											<div class="tg-service">
												<span class="tg-seviceicon">
													<i class="fa fa-heartbeat"></i>
												</span>
												<div class="tg-heading">
													<h4><a href="#">ประกันสุขภาพ</a></h4>
												</div>
												<div class="tg-description">
													<p>ที่สุดของแผนประกันสุขภาพ คุ้มครอง ครอบคลุมทุกความต้องการ ทุกที่ ทุกเวลา</p>
							
												</div>
											</div>
										</div>
									</div>
									<div class="col-sm-6">
										<div class="tg-box">
											<div class="tg-service">
												<span class="tg-seviceicon">
													<i class="fa fa-shield"></i>
												</span>
												<div class="tg-heading">
													<h4><a href="#">ประกันคุ้มครองรายได้</a></h4>
												</div>
												<div class="tg-description">
													<p>ใครจะดูแล“คนข้างหลัง”ของคุณได้ดีเท่ากับสี่งที่คุณเตรียมไว้ให้พวกเขาเอง</p>
								</div>
											</div>
										</div>
									</div>
									<div class="col-sm-6">
										<div class="tg-box">
											<div class="tg-service">
												<span class="tg-seviceicon">
													<i class="fa fa-line-chart"></i>
												</span>
												<div class="tg-heading">
													<h4><a href="#">ประกันสะสมทรัพย์</a></h4>
												</div>
												<div class="tg-description">
														<p>ไม่มีคำว่าเร็วเกินไปสำหรับการสร้างรากฐานทางการเงินที่มั่นคง
</p>
								</div>
											</div>
										</div>
									</div>
									<div class="col-sm-6">
										<div class="tg-box">
											<div class="tg-service">
												<span class="tg-seviceicon">
													<i class="fa fa-balance-scale"></i>
												</span>
												<div class="tg-heading">
													<h4><a href="#">ประกันชีวิตแบบบำนาญ</a></h4>
												</div>
												<div class="tg-description">
												<p>เพราะการเกษียณไม่ใช่จุดสิ้นสุดของชีวิต แต่เป็นจุดเริ่มต้นของการใช้ชีวิต</p>
									</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-sm-4 hidden-sm hidden-xs">
							<figure class="tg-serviceimg">
								<img src="images/img-03.jpg" alt="image description">
							</figure>
						</div>
					</div>
				</div>
			</section>

		<section class="tg-bglight tg-haslayout">
				<div class="col-sm-6">
					<div class="row">
						<div class="tg-videosection">
							<figure>
								<img src="images/img-01.jpg" alt="image description">
								<figcaption>
									<a class="tg-btnplay" href="#"><i class="fa fa-play"></i></a>
									<h2>"เพราะการปกป้องที่ดีที่สุด คือการปล่อยให้เค้าเรียนรู้"</h2>
								</figcaption>
							</figure>
						</div>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="tg-whychooseus">
						<div class="tg-sectionhead">
							<div class="tg-sectiontitle">
								<h2>แผนประกันที่เหมาะสมและตรงกับความต้องการของคุณ</h2>
										<h3 style="text-align: center;">กรุงไทย-แอกซ่า ประกันชีวิต</h3>
							</div>
							<div class="tg-description">
								<p>อยู่เคียงข้างคุณในทุกก้าวย่างของชีวิต ตั้งแต่จบการศึกษา แต่งงาน มีลูก ไปจนถึงการวางแผนเกษียณอายุ โดยมอบแบบประกัน
								ที่คลอบคลุมทุกความต้องการที่จะช่วยดูแลคนที่คุณรัก เราพร้อมดูแลคุณทั้งชีวิต</p>
								
						</br>
								<h4>- Reliable ไว้ใจได้</h4></br>
										<h5>เราทำในสิ่งที่พูด และเราพูดในสิ่งที่เราจะทำ คุณจึงไว้ใจเราได้</h5></br>
									<h4>- Available พร้อมให้บริการ</h4>
								</br><h5>เรารับฟังลูกค้าอย่างแท้จริงและเราพร้อมที่จะให้บริการทุกที่ทุกเวลาที่ลูกค้าต้องการ
</h5>	</br>	
<h4>- Attentive ใส่ใจลูกค้าเสมอ</h4>
								</br>
								<h5>เราปฏิบัติต่อลูกค้าด้วยความเข้าใจและเอาใจใส่ต่อความต้องการเฉพาะบุคคล</h5>
							
							</div>
						</div>
				
					</div>
				</div>
			</section>
			
			<?php include('footerb.php'); ?>