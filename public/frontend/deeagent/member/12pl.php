<?php include('header.php'); ?>
		
			<main id="tg-main" class="tg-main tg-haslayout">
			<div class="col-sm-12 col-xs-12">
						</br> </br></br></br></br></br>
						<div class="tg-sectiontitle">
													<h2>12 PL สมาร์ทโปร
สะสมเบี้ย 12 ปี คุ้มครองนาน
เพื่อเป็นทุนและมรดก</h2>
													<h3>สะสมเบี้ย 12 ปี คุ้มครองยาวนาน</h3>
												</div></br></br>
							<div class="container">
					<div class="row">
						<div class="tg-member tg-detailpage">
							<div class="col-sm-4 col-xs-12 pull-left">
								<figure><img src="images/events/img-03.jpg" alt="image description">
								</figure></div>
								
							<div class="col-sm-8 col-xs-12 pull-left">
	
										<div class="tg-sectionhead">
									<div class="tg-sectiontitle">
										
												<p>ความคุ้มครองที่สูง สะสมเพื่อเป็นกองทุนมรดก
ชำระเบี้ยประกันเพียง 12 ปี คุ้มครองถึงอายุ 85 ปี ด้วยเบี้ยประกันที่ต่ำ และให้ความคุ้มครองสูง มีเงินจ่ายคืนประจำปี ตั้งแต่ปีที่ 2 
จนครบอายุ 85 จะดีกว่าไหมหากเราสามารถเปลี่ยนค่าใช้จ่ายให้เป็นเงินออมเพื่ออนาคตของคุณและครอบครัว เริ่มวางแผนและรับตารางผลประโยชน์และความคุ้มครองประกอบการตัดสินใจได้ที่นี่</p>
<p>- สะสมเพื่อเป็นกองทุนมรดก เงินคืนตั้งแต่ปีที่ 2 ถึงครบอายุ 85 สามารถสะสมเป็นกองทุนมรดกในยามเกษียนอายุ</p>
<p>- มั่นใจกับอนาคตที่มั่นคง รับเงินครบกำหนดสัญญา 115% ของจำนวนเงินเอาประกันี่</p>
<p>- คุ้มครองชีวิต 100% ของจำนวนเงิเอาประกันตลอดสัญญา หรือมูลค่าเวนคืนเงินสดและแต่จำนวนใดที่มากกว่าี่</p>
									</div>
									
								</div>

													</div>
							<div class="col-sm-12 col-xs-12 pull-right">
						<img src="https://www.puthanavit1r.com/images/12PL-krungthai-axa.jpg" alt="image description">
								<div class="tg-memberinfo">
									<span>ผลประโยชน์และความคุ้มครอง</span>
									<span>เงื่อนไขการรับประกัน</span>
									<span>รับเงินจ่ายคืนประจำปีทุกปีโดยเริ่ม I สิ้นปีที่ 2 เป็นต้นไปจนกระทั่งครบอายุ 85 ปี</span>
									<span>อายุขณะทำสัญญาประกันภัยปีแรกตั้งแต่ 1 เดือน - 60 ปี</span>
									<span>สิ้นปีที่ 5 จะได้รับเงินคืนประจำปีเป็นจำนวนเงิน 4,000 บาท</span>
									<span>จำนวนเงินเอาประกันภัยขั้นต่ำ 100,000 บาท</span>
									<span>สิ้นปีที่ 15 จะได้รับเงินคืนประจำปีเป็นจำนวนเงิน 9,000 บาท</span>
									<span>สามารถซื้อสัญญาเพิ่มเติมได้ตามกฎเกณฑ์การรับประกันของสัญญาเพิ่มเติมนั้นๆ </span>
									<span>สิ้นปีที่ 17-50 จะได้รับเงินคืนประจำปีเป็นจำนวนเงิน 10,000 บาท</span>
									<span>เบี้ยประกันภัยสำหรับแบบประกัน 12PL มาหักลดหย่อนภาษีเงินได้บุคคลธรรมดาที่จ่ายจริงแต่ไม่เกินจำนวนสูงสุดที่กรมสรรพากรกำหนด</span>
									<span>อายุครบ 85 ปี รับเงิน 115% ของทุนประกันภัย 1,150,000 บาท</span>
									<span>เบี้ยประกันภัยสำหรับแบบประกัน 12PL </span>
									<span>รับสิทธินำเบี้ยประกันไปหักลดหย่อนภาษีเงินได้บุคคลธรรมดาสูงสุดถึง100,000 บาท</span>
									<span>เงินปันผลตามกรมธรรม์เป็นผลประโยชน์ที่ไม่รองรับและสามารถเปลี่ยนแปลงได้</span>
								</div>	
												<div class="tg-widgetcontent">
												<div class="tg-sectiontitle">
									</div>	
								
					</div>
				</div>
			</div><div id="tg-twocolumns" class="tg-twocolumns">
					
								
							</div>
						</div><div class="tg-pagecontent">
				</div></div></div>
			
		</main>
			

	<?php include('footerb.php'); ?>