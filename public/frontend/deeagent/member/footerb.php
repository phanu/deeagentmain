<section class="tg-main-section tg-haslayout">
		<div class="container">
				
					<div class="tg-widgetcontent">
												<div class="tg-sectiontitle">
							<h2>รับรายละเอียด เบี้ยประกัน</h2>
									</div>	
													<form class="tg-themeform">
														<fieldset>
							<div class="form-group">
								<div class="tg-select">
								<select>
									<option>เลือกแบบประกัน</option>
									<option>ประกันสุขภาพเด็ก 0 -15 ปี</option>
									<option>ประกันสุขภาพ</option>
									<option>ประกันสะสมทรัพย์</option>
									<option>ประกันบำนาญ</option>
									<option>ประกันคุ้มครองรายได้ หรือมรดก</option>

								</select>
							</div>
							</div>
							<div class="form-group">
								<div class="tg-select">
								<select>
																	<option>อายุ</option>
																		<option> 1 เดือน</option>
																		<option> 2 เดือน</option>
																		<option> 3 เดือน</option>
																	</select>
																</div>
															</div>
															<div class="form-group">
																<div class="tg-select">
																	<select>
																	<option>เลือกจังหวัด</option>
																		<option> กรุงเทพมหานคร</option>
																		<option> กระบี่</option>
																		<option> กาญจนบุรี</option>
																	</select>
																</div>
															</div>
															<div class="form-group">
																<input type="text" class="form-control" name="" placeholder="ชื่อ-นามสกุล">
															</div>
															<div class="form-group">
																<input type="text" class="form-control" name="" placeholder="เบอร์โทรศัพท์">
															</div>
															<div class="form-group">
																<input type="text" class="form-control" name="" placeholder="อีเมล">
															</div>
															
															<button class="tg-btn" type="submit"><span>รับเลย</span></button>
														</fieldset>
													</form>
												</div>
				</div>
				
			</section>
			<footer id="tg-footer" class="tg-footer tg-haslayout">
	
			<div class="tg-footermiddlearea">
				<div class="container">
					<div class="row">
						<div class="tg-footerwidgets">
							<div class="col-md-3 col-sm-6 col-xs-6">
								<div class="tg-widget tg-widgetinfo">
								<div class="tg-member tg-detailpage">
							<div class="col-sm-12 col-xs-12 pull-left">
								<figure><img src="images/team/img-14.jpg" alt="image description"></figure>

								

													</div>
							
						</div>
									
									<ul class="tg-contactinfo">
										<li>
											<i class="fa fa-user"></i>
											<address>พรรษกาล ธุระมนวงศ์</address>
										</li>
										<div class="tg-sectiontitle">
										
										
									</div>
										
										<li>
											<i class="fa fa-phone"></i>
											<span>098 252 8986</span>
										</li>
									<li>
											<i class="fa fa-envelope-o"></i>
											<span>jiggjapp@gmail.com</span>
										</li></ul>
								</div>
							</div>
							<div class="col-md-3 col-sm-6 col-xs-6">
								<div class="tg-widget tg-widgetusefulllinks">
									<div class="tg-widgettitle"><h3>ประกันสุขภาพ</h3></div>
									<ul>
										<li><a href="#">ไอชายด์ เฮลท์ โพรเทคชั่น</a></li>
										<li><a href="#">เพอร์เฟคเฮลท์ โซลูชั่น</a></li>
										<li><a href="#">ประกันสุขภาพเด็ก อายุ 0-15 ปี</a></li>
										<li><a href="#">ประกันสุขภาพ สุขภาพดี 6200</a></li>
											<li><a href="#">ประกันสุขภาพ วัยทำงาน</a></li>
												<li><a href="#">ประกันสุขภาพ แบบออม 12 ปีี</a></li>
									<!-- 	
									
										<li><a href="#">ประกันสุขภาพ แบบออม 20 ปี</a></li>
										<li><a href="#">ประกันโรคร้ายแรง i-Care</a></li>-->
								
									</ul>
								</div>
							</div>
			
							<div class="col-md-3 col-sm-6 col-xs-6">
								<div class="tg-widget tg-widgetusefulllinks">
									<div class="tg-widgettitle"><h3>ประกันออมทรัพย์</h3></div>
									<ul>
										<li><a href="#">ประกันทุนการศึกษาบุตร iGrow</a></li>
										<li><a href="#">ประกันออมทรัพย์ iPlus</a></li>
										<li><a href="#">ประกันออมทรัพย์ 20SS</a></li>
						
									</ul>
									<div class="tg-widgettitle"><h3>ประกันคุ้มครองรายได้</h3></div>
									<ul>
										<li><a href="#">ประกันคุ้มครองรายได้ | iProtect</a></li>
										<li><a href="#">ประกันคุ้มครองชีวิตและอุบัติเหตุ | iFine</a></li>
								
						
								
									</ul>
								</div>
							</div>
						<div class="col-md-3 col-sm-6 col-xs-6">
								<div class="tg-widget tg-widgetusefulllinks">
									<div class="tg-widgettitle"><h3>ประกันแบบบำนาญ</h3></div>
									<ul>
										<li><a href="#">ประกันบำนาญ i-Wish 10</a></li>
										<li><a href="#">ประกันบำนาญ i-Wish 55</a></li>
										<li><a href="#">ประกันบำนาญ i-Wish 60</a></li>
										<li><a href="#">ประกันบำนาญ i-Wish 65</a></li>
							
								
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="tg-footerbottombar">
				<div class="container">
					<div class="row">
						<div class="col-sm-12 col-xs-12">
							<span class="tg-copyright">ลิขสิทธิ์ถูกต้อง © 2016 DEEAGENT</span>
							<ul class="tg-socialicons">
								<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
								<li><a href="#"><i class="fa fa-twitter"></i></a></li>
								<li><a href="#"><i class="fa fa-facebook-f"></i></a></li>
								<li><a href="#"><i class="fa fa-pinterest-p"></i></a></li>
								<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
							</ul>
							<strong class="tg-logo"><a href="#"><img src="images/deeagentw.svg" alt="image description"></a></strong>
						</div>
					</div>
				</div>
			</div>
		</footer>
		
			<script src="js/vendor/jquery-library.js"></script>
	<script src="js/vendor/bootstrap.min.js"></script>
	<script src="http://maps.google.com/maps/api/js?key=AIzaSyCR-KEWAVCn52mSdeVeTqZjtqbmVJyfSus&language=en"></script>
	<script src="js/owl.carousel.min.js"></script>
	<script src="js/jquery.svgInject.js"></script>
	<script src="js/isotope.pkgd.js"></script>
	<script src="js/chartsloader.js"></script>
	<script src="js/parallax.js"></script>
	<script src="js/countTo.js"></script>
	<script src="js/appear.js"></script>
	<script src="js/gmap3.js"></script>
	<script src="js/main.js"></script>