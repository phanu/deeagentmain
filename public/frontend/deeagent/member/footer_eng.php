<div class="rs_graybg rs_toppadder60 rs_bottompadder60">
	<div class="container">
		<div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="rs_index2_footerdiv"> <img src="images/banner/banner16.svg" class="img-responsive" alt="logo"> </div>
        
        
      </div>
    </div>
	</div>
</div>

<div class="rs_testimonial_section rs_toppadder50 rs_bottompadder50">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="row">
          <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 col-lg-offset-3 col-md-offset-3">
            <div class="row">
              <div class="rs_subscribe_section_form rs_toppadder30">
                <form class="form">
                  <input class="form-control" type="text" placeholder="Free bid"> <a href="register.php" class="rs_button rs_button_orange pull-right rs_center_btn">สมัครสมาชิก</a>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="rs_testimonial_section2 rs_toppadder50 rs_bottompadder50">
</div>

<div class="rs_footer rs_toppadder60 rs_bottompadder60">
	<div class="container">
		<div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="rs_index2_footerdiv"> <img src="images/sibezablack.svg" class="img-responsive" alt="logo"> </div>
        
        <div class="rs_index2_footerdiv">
          <div class="rs_footer_textdata">
            <p><i class="fa fa-map-marker"></i> Sibeza Co.,Ltd. </p>
            <p> 350/53 pharam 3 road soi 71 chong-non-see bangkok 10120</p>
            <p><i class="fa fa-phone"></i> 02 xxx xxxx </p>
            <p><a href="#"><i class="fa fa-envelope"></i> info@sibeza.com</a></p>
          </div>
        </div>
      </div>
    </div>
	</div>
</div>

<div class="rs_topfooterwrapper rs_toppadder20 rs_bottompadder20">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="rs_footersocial">
					<ul>
						<li><a href="#"><i class="fa fa-facebook-square"></i></a></li>
						<li><a href="#"><i class="fa fa-twitter-square"></i></a></li>
						<li><a href="#"><i class="fa fa-skype"></i></a></li>
						<li><a href="#"><i class="fa fa-instagram"></i></a></li>
						<li><a href="#"><i class="fa fa-youtube-square"></i></a></li>
						<li><a href="#"><i class="fa fa-vimeo-square"></i></a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>


<!-- Script Start --> 
<script src="js/jquery-1.12.3.min.js" type="text/javascript"></script> 
<!--<script src="js/jquery-1.11.3.min.js" type="text/javascript"></script> -->
<script src="js/bootstrap.js" type="text/javascript"></script> 
<script src="js/modernizr.custom.js" type="text/javascript"></script> 
<script src="js/plugins/rating/star-rating.js" type="text/javascript"></script> 
<script src="js/plugins/countto/jquery.countTo.js" type="text/javascript"></script> 
<script src="js/plugins/countto/jquery.appear.js" type="text/javascript"></script> 
<script src="js/plugins/owl/owl.carousel.js" type="text/javascript"></script> 
<script src="js/plugins/revel/jquery.themepunch.tools.min.js" type="text/javascript"></script> 
<script src="js/plugins/revel/jquery.themepunch.revolution.min.js" type="text/javascript"></script> 
<script src="js/plugins/revel/revolution.extension.actions.min.js" type="text/javascript"></script> 
<script src="js/plugins/revel/revolution.extension.layeranimation.min.js" type="text/javascript"></script> 
<script src="js/plugins/revel/revolution.extension.navigation.min.js" type="text/javascript"></script> 
<script src="js/plugins/revel/revolution.extension.parallax.min.js" type="text/javascript"></script> 
<script src="js/plugins/revel/revolution.extension.slideanims.min.js" type="text/javascript"></script> 
<script src="js/jquery.mixitup.js" type="text/javascript"></script> 
<script src="js/plugins/fancybox/jquery.fancybox.js" type="text/javascript"></script> 
<script src="js/plugins/fancybox/jquery.fancybox.pack.js" type="text/javascript"></script> 
<script src="js/plugins/bootstrap-slider/bootstrap-slider.js" type="text/javascript"></script> 
<script src="js/plugins/offcanvasmenu/snap.svg-min.js" type="text/javascript"></script> 
<script src="js/plugins/offcanvasmenu/classie.js" type="text/javascript"></script> 
<script src="js/plugins/offcanvasmenu/main3.js" type="text/javascript"></script> 
<script src="js/plugins/jquery-ui/jquery-ui.js" type="text/javascript"></script> 
<script src="js/plugins/c3_chart/d3.v3.min.js" type="text/javascript"></script> 
<script src="js/plugins/c3_chart/c3.js" type="text/javascript"></script> 
<script src="js/pgwslideshow.js" type="text/javascript"></script> 
<script src="js/custom.js" type="text/javascript"></script> 
<!-- Script end -->
<script src="js/LivIconsEvo.Tools.js"></script>
<script src="js/LivIconsEvo.defaults.js"></script>
<script src="js/LivIconsEvo.min.js"></script>
<script type="text/javascript" src="https://www.sibeza.com/restored/chat/php/app.php?widget-init.js"></script>
</body>