<?php include('header.php'); ?>
<body>

		<div id="tg-innerbanner" class="tg-innerbanner tg-haslayout">
			<div class="container">
				<div class="row">
					<div class="col-sm-12">
						<div class="tg-innerbannercontent">
							<div class="tg-pagetitle">
								<h1>ประกันคุ้มครองรายได้</h1>
							</div>
						
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--************************************
				Inner Banner End
		*************************************-->
		<!--************************************
				Main Start
		*************************************-->
		<main id="tg-main" class="tg-main tg-haslayout">
			<div class="tg-pagecontent">
				<div class="container">
					<div class="row">
						<div class="col-lg-offset-2 col-lg-8 col-md-offset-1 col-md-10 col-sm-offset-0 col-sm-12 col-xs-12">
							<div class="tg-sectionhead">
								<div class="tg-sectiontitle">
									<h2>กรุงไทย-แอกซ่า ประกันชีวิต</h2>
									<h3>ประกันชีวิตแบบบำนาญ</h3>
								</div>
								<div class="tg-description">
									<p>กรุงไทย-แอกซ่า ประกันชีวิต มุ่งมั่นที่จะช่วยคุณวางแผนการเกษียณอายุที่คลอบคลุมรอบด้าน เพื่อสร้างความมั่งคั่งทางการเงินและทำให้คุณอุ่นใจด้วยความคุ้มครองสุขภาพไปพร้อมๆ กัน ให้คุณใช้ชีวิตในวัยเกษียณดั่งใจฝัน ไม่ว่าจะเป็นการท่องเที่ยวไปทั่วโลก การพักผ่อนหย่อนใจ หรือการใช้ชีวิตแบบสบายๆ ที่ห้อมล้อมไปด้วยคนที่คุณรัก เราสามารถช่วยคุณวางแผนเพื่อบรรลุเป้าหมายวัยเกษียณที่คุณต้องการได้</p>
								</div>
							</div>
						</div>
						<div class="tg-events">
							<div class="col-md-4 col-sm-6 col-xs-12">
								<div class="tg-event">
									<figure>
										<a href="#"><img src="images/events/img-04.jpg" alt="image description"></a>
										<figcaption>
											<ul class="tg-eventmetadata">
												<li><a href="#">ค่าเบี้ยสบายกระเป๋าเพียงหลักร้อยต่อเดือน*</a></li>
												<li><a href="#">เบี้ยประกันหักลดหย่อนภาษีได้**้</a></li>
											</ul>
										</figcaption>
									</figure>
									<div class="tg-eventcontent tg-borderstyle">
										<div class="tg-eventtitle">
											<h2><a href="#">ประกันชีวิตแบบบำนาญ | iWish</a></h2>
										</div>
										<div class="tg-description">
											<p>เพราะความแน่นอนคือความไม่แน่นอน และหากมีสิ่งไม่คาดฝันเกิดขึ้นกับคุณ ใครจะดูแล “คนข้างหลัง” ของคุณได้ดีเท่ากับสี่งที่คุณเตรียมไว้ให้พวกเขาเอง</p>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-sm-6 col-xs-12">
								<div class="tg-event">
									<figure>
										<a href="#"><img src="images/events/img-04.jpg" alt="image description"></a>
										<figcaption>
											<ul class="tg-eventmetadata">
										<li><a href="#">คุ้มครองการเสียชีวิตทุกกรณี สูงสุด 300,000บาท</a></li>
												<li><a href="#">กรณีเสียชีวิต* และเพิ่มเป็น 2 เท่า</a></li>
											</ul>
										</figcaption>
									</figure>
									<div class="tg-eventcontent tg-borderstyle">
										<div class="tg-eventtitle">
											<h2><a href="#">ประกันคุ้มครองชีวิตและอุบัติเหตุ | iFine</a></h2>
										</div>
										<div class="tg-description">
											<p>ประกันไอไฟน์ iFine สบายๆ ให้ความคุ้มครองชีวิตและอุบัติเหตุแบบประกันที่ให้ความคุ้มครองชีวิตและอุบัติเหตุในกรมธรรม์เดียว ครอบคลุมอุบัติเหตุหลากหลายอย่างคุ้มค่า 
										</p>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-sm-6 col-xs-12">
								<div class="tg-event">
									<figure>
										<a href="#"><img src="images/events/img-04.jpg" alt="image description"></a>
										<figcaption>
											<ul class="tg-eventmetadata">
												<li><a href="#">มั่นใจกับอนาคตที่มั่นคง รับเงินครบกำหนดสัญญา 115%</a></li>
												<li><a href="#">ความคุ้มครองที่สูง สะสมเพื่อเป็นกองทุนมรดก</a></li>
											</ul>
										</figcaption>
									</figure>
									<div class="tg-eventcontent tg-borderstyle">
										<div class="tg-eventtitle">
											<h2><a href="#">สะสมเบี้ย 12 ปี คุ้มครองยาวนาน | Smart Pro</a></h2>
										</div>
										<div class="tg-description">
											<p>จะดีกว่าไหมหากเราสามารถเปลี่ยนค่าใช้จ่ายให้เป็นเงินออมเพื่ออนาคตของคุณและครอบครัว เริ่มวางแผนและรับตารางผลประโยชน์และความคุ้มครองประกอบการตัดสินใจได้ที่นี่</p>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-sm-6 col-xs-12">
								<div class="tg-event">
									<figure>
										<a href="#"><img src="images/events/img-04.jpg" alt="image description"></a>
										<figcaption>
											<ul class="tg-eventmetadata">
												<li><a href="#">June 27, 2016</a></li>
												<li><a href="#">09:00 am</a></li>
												<li><a href="#">Manchester, UK</a></li>
											</ul>
										</figcaption>
									</figure>
									<div class="tg-eventcontent tg-borderstyle">
										<div class="tg-eventtitle">
											<h2><a href="#">adipisicing elit eiusmod tempor adipisicing elit eiusmod tempor</a></h2>
										</div>
										<div class="tg-description">
											<p>Consectetur adipisicing elit sedo eiusmod tempor dunt ut labore et dolore magna aliqua.</p>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-sm-6 col-xs-12">
								<div class="tg-event">
									<figure>
										<a href="#"><img src="images/events/img-04.jpg" alt="image description"></a>
										<figcaption>
											<ul class="tg-eventmetadata">
												<li><a href="#">June 27, 2016</a></li>
												<li><a href="#">09:00 am</a></li>
												<li><a href="#">Manchester, UK</a></li>
											</ul>
										</figcaption>
									</figure>
									<div class="tg-eventcontent tg-borderstyle">
										<div class="tg-eventtitle">
											<h2><a href="#">adipisicing elit eiusmod tempor adipisicing elit eiusmod tempor</a></h2>
										</div>
										<div class="tg-description">
											<p>Consectetur adipisicing elit sedo eiusmod tempor dunt ut labore et dolore magna aliqua.</p>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-sm-6 col-xs-12">
								<div class="tg-event">
									<figure>
										<a href="#"><img src="images/events/img-04.jpg" alt="image description"></a>
										<figcaption>
											<ul class="tg-eventmetadata">
												<li><a href="#">June 27, 2016</a></li>
												<li><a href="#">09:00 am</a></li>
												<li><a href="#">Manchester, UK</a></li>
											</ul>
										</figcaption>
									</figure>
									<div class="tg-eventcontent tg-borderstyle">
										<div class="tg-eventtitle">
											<h2><a href="#">adipisicing elit eiusmod tempor adipisicing elit eiusmod tempor</a></h2>
										</div>
										<div class="tg-description">
											<p>Consectetur adipisicing elit sedo eiusmod tempor dunt ut labore et dolore magna aliqua.</p>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-sm-6 col-xs-12">
								<div class="tg-event">
									<figure>
										<a href="#"><img src="images/events/img-04.jpg" alt="image description"></a>
										<figcaption>
											<ul class="tg-eventmetadata">
												<li><a href="#">June 27, 2016</a></li>
												<li><a href="#">09:00 am</a></li>
												<li><a href="#">Manchester, UK</a></li>
											</ul>
										</figcaption>
									</figure>
									<div class="tg-eventcontent tg-borderstyle">
										<div class="tg-eventtitle">
											<h2><a href="#">adipisicing elit eiusmod tempor adipisicing elit eiusmod tempor</a></h2>
										</div>
										<div class="tg-description">
											<p>Consectetur adipisicing elit sedo eiusmod tempor dunt ut labore et dolore magna aliqua.</p>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-sm-6 col-xs-12">
								<div class="tg-event">
									<figure>
										<a href="#"><img src="images/events/img-04.jpg" alt="image description"></a>
										<figcaption>
											<ul class="tg-eventmetadata">
												<li><a href="#">June 27, 2016</a></li>
												<li><a href="#">09:00 am</a></li>
												<li><a href="#">Manchester, UK</a></li>
											</ul>
										</figcaption>
									</figure>
									<div class="tg-eventcontent tg-borderstyle">
										<div class="tg-eventtitle">
											<h2><a href="#">adipisicing elit eiusmod tempor adipisicing elit eiusmod tempor</a></h2>
										</div>
										<div class="tg-description">
											<p>Consectetur adipisicing elit sedo eiusmod tempor dunt ut labore et dolore magna aliqua.</p>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-sm-6 col-xs-12">
								<div class="tg-event">
									<figure>
										<a href="#"><img src="images/events/img-04.jpg" alt="image description"></a>
										<figcaption>
											<ul class="tg-eventmetadata">
												<li><a href="#">June 27, 2016</a></li>
												<li><a href="#">09:00 am</a></li>
												<li><a href="#">Manchester, UK</a></li>
											</ul>
										</figcaption>
									</figure>
									<div class="tg-eventcontent tg-borderstyle">
										<div class="tg-eventtitle">
											<h2><a href="#">adipisicing elit eiusmod tempor adipisicing elit eiusmod tempor</a></h2>
										</div>
										<div class="tg-description">
											<p>Consectetur adipisicing elit sedo eiusmod tempor dunt ut labore et dolore magna aliqua.</p>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-sm-12 col-xs-12">
							<nav class="tg-pagination">
								<ul>
									<li class="tg-prevpage"><a href="#"><i class="fa fa-angle-left"></i></a></li>
									<li><a href="#">1</a></li>
									<li><a href="#">2</a></li>
									<li><a href="#">3</a></li>
									<li><a href="#">4</a></li>
									<li><a href="#">5</a></li>
									<li>...</li>
									<li><a href="#">10</a></li>
									<li class="tg-nextpage"><a href="#"><i class="fa fa-angle-right"></i></a></li>
								</ul>
							</nav>
						</div>
					</div>
				</div>
			</div>
		</main>
			

	<?php include('footerb.php'); ?>