<section class="tg-main-section tg-haslayout">
		<div class="container">
				
	<div class="tg-widgetcontent">
						<div class="tg-sectiontitle">
							<h2>รับรายละเอียด เบี้ยประกัน</h2>
						</div>	
					<form class="tg-themeform">
		<fieldset>
							<div class="form-group">
								<div class="tg-select">
								<select>
									<option>เลือกแบบประกัน</option>
									<option>ประกันสุขภาพเด็ก 0 -15 ปี</option>
									<option>ประกันสุขภาพ</option>
									<option>ประกันสะสมทรัพย์</option>
									<option>ประกันบำนาญ</option>
									<option>ประกันคุ้มครองรายได้ หรือมรดก</option>

								</select>
							</div>
							</div>
							<div class="form-group">
								<div class="tg-select">
								<select>
									<option>อายุ</option>
									<option> 1 เดือน</option>
									<option> 2 เดือน</option>
									<option> 3 เดือน</option>
								</select>
								</div>
							</div>
							<div class="form-group">
								<div class="tg-select">
								<select>
								<option>เลือกจังหวัด</option>
									<option> กรุงเทพมหานคร</option>
									<option> กระบี่</option>
									<option> กาญจนบุรี</option>
								</select>
								</div>
								</div>
							<div class="form-group">
								<input type="text" class="form-control" name="" placeholder="ชื่อ-นามสกุล">
							</div>
								<div class="form-group">
								<input type="text" class="form-control" name="" placeholder="เบอร์โทรศัพท์">
							</div>
							<div class="form-group">
								<input type="text" class="form-control" name="" placeholder="อีเมล">
							</div>
								<button class="tg-btn" type="submit"><span>รับเลย</span></button>
							</fieldset>
													</form>
												</div>
				</div>
				
			</section>
			<footer id="tg-footer" class="tg-footer tg-haslayout">
	
			<div class="tg-footermiddlearea">
				<div class="container">
					<div class="row">
						<div class="tg-footerwidgets">
							<div class="col-md-3 col-sm-6 col-xs-6">
								<div class="tg-widget tg-widgetinfo">
								<div id="tg-footerlocation-map" class="tg-footerlocation-map" 
								style="position: relative; overflow: hidden;">
								<div style="height: 100%; width: 100%; position: absolute; 
								background-color: rgb(229, 227, 223);"><div class="gm-style" 
								style="position: absolute; left: 0px; top: 0px; overflow: hidden; width: 100%; 
								height: 100%; z-index: 0;">
								<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d242.1407494464911!2d100.65652863157182!3d13.823901501504364!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x311d62fe45948dc7%3A0x58228884a445f68c!2z4LmA4LiU4Lit4Liw4LmB4Lie4Lil4LiZ4LiX4LmMIOC4meC4p-C4oeC4tOC4meC4l-C4o-C5jCAoVGhlIFBsYW50IE5hd2FtaW4p!5e0!3m2!1sth!2sth!4v1476828443394" width="600" height="450" frameborder="0" style="border:0" allowfullscreen=""></iframe><div style="margin-left: 5px; margin-right: 5px; z-index: 1000000; position: absolute; left: 0px; bottom: 0px;"><a target="_blank" href="https://maps.google.com/maps?ll=53.489276,-2.317081&amp;z=16&amp;t=m&amp;hl=en&amp;gl=US&amp;mapclient=apiv3" title="Click to see this area on Google Maps" style="position: static; overflow: visible; float: none; display: inline;"><div style="width: 66px; height: 26px; cursor: pointer;"><img src="http://maps.gstatic.com/mapfiles/api-3/images/google4.png" draggable="false" style="position: absolute; left: 0px; top: 0px; width: 66px; height: 26px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px;"></div></a></div><div style="background-color: white; padding: 15px 21px; border: 1px solid rgb(171, 171, 171); font-family: Roboto, Arial, sans-serif; color: rgb(34, 34, 34); box-shadow: rgba(0, 0, 0, 0.2) 0px 4px 16px; z-index: 10000002; display: none; width: 194px; height: 88px; position: absolute; left: 5px; top: 5px;"><div style="padding: 0px 0px 10px; font-size: 16px;">Map Data</div><div style="font-size: 13px;">Map data ©2016 Google</div><div style="width: 13px; height: 13px; overflow: hidden; position: absolute; opacity: 0.7; right: 12px; top: 12px; z-index: 10000; cursor: pointer;"><img src="http://maps.gstatic.com/mapfiles/api-3/images/mapcnt6.png" draggable="false" style="position: absolute; left: -2px; top: -336px; width: 59px; height: 492px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div></div><div class="gmnoprint" style="z-index: 1000001; position: absolute; right: 0px; bottom: 0px; width: 12px;"><div draggable="false" class="gm-style-cc" style="-webkit-user-select: none; height: 14px; line-height: 14px;"><div style="opacity: 0.7; width: 100%; height: 100%; position: absolute;"><div style="width: 1px;"></div><div style="background-color: rgb(245, 245, 245); width: auto; height: 100%; margin-left: 1px;"></div></div><div style="position: relative; padding-right: 6px; padding-left: 6px; font-family: Roboto, Arial, sans-serif; font-size: 10px; color: rgb(68, 68, 68); white-space: nowrap; direction: ltr; text-align: right; vertical-align: middle; display: inline-block;"><a style="color: rgb(68, 68, 68); text-decoration: none; cursor: pointer; display: none;">Map Data</a><span>Map data ©2016 Google</span></div></div></div><div class="gmnoscreen" style="position: absolute; right: 0px; bottom: 0px;"><div style="font-family: Roboto, Arial, sans-serif; font-size: 11px; color: rgb(68, 68, 68); direction: ltr; text-align: right; background-color: rgb(245, 245, 245);">Map data ©2016 Google</div></div><div class="gmnoprint gm-style-cc" draggable="false" style="z-index: 1000001; -webkit-user-select: none; height: 14px; line-height: 14px; position: absolute; right: 0px; bottom: 0px;"><div style="opacity: 0.7; width: 100%; height: 100%; position: absolute;"><div style="width: 1px;"></div><div style="background-color: rgb(245, 245, 245); width: auto; height: 100%; margin-left: 1px;"></div></div><div style="position: relative; padding-right: 6px; padding-left: 6px; font-family: Roboto, Arial, sans-serif; font-size: 10px; color: rgb(68, 68, 68); white-space: nowrap; direction: ltr; text-align: right; vertical-align: middle; display: inline-block;"><a href="https://www.google.com/intl/en_US/help/terms_maps.html" target="_blank" style="text-decoration: none; cursor: pointer; color: rgb(68, 68, 68);">Terms of Use</a></div></div><div style="width: 25px; height: 25px; overflow: hidden; display: none; margin: 10px 14px; position: absolute; top: 0px; right: 0px;"><img src="http://maps.gstatic.com/mapfiles/api-3/images/sv9.png" draggable="false" class="gm-fullscreen-control" style="position: absolute; left: -52px; top: -86px; width: 164px; height: 175px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px;"></div><div draggable="false" class="gm-style-cc" style="-webkit-user-select: none; height: 14px; line-height: 14px; display: none; position: absolute; right: 0px; bottom: 0px;"><div style="opacity: 0.7; width: 100%; height: 100%; position: absolute;"><div style="width: 1px;"></div><div style="background-color: rgb(245, 245, 245); width: auto; height: 100%; margin-left: 1px;"></div></div><div style="position: relative; padding-right: 6px; padding-left: 6px; font-family: Roboto, Arial, sans-serif; font-size: 10px; color: rgb(68, 68, 68); white-space: nowrap; direction: ltr; text-align: right; vertical-align: middle; display: inline-block;"><a target="_new" title="Report errors in the road map or imagery to Google" href="https://www.google.com/maps/@53.4892759,-2.3170814,16z/data=!10m1!1e1!12b1?source=apiv3&amp;rapsrc=apiv3" style="font-family: Roboto, Arial, sans-serif; font-size: 10px; color: rgb(68, 68, 68); text-decoration: none; position: relative;">Report a map error</a></div></div><div class="gmnoprint gm-bundled-control gm-bundled-control-on-bottom" draggable="false" controlwidth="0" controlheight="0" style="margin: 10px; -webkit-user-select: none; position: absolute; display: none; bottom: 0px; right: 0px;"><div class="gmnoprint" style="display: none; position: absolute;"><div draggable="false" style="-webkit-user-select: none; box-shadow: rgba(0, 0, 0, 0.298039) 0px 1px 4px -1px; border-radius: 2px; cursor: pointer; background-color: rgb(255, 255, 255);"><div title="Zoom in" style="position: relative;"><div style="overflow: hidden; position: absolute;"><img src="http://maps.gstatic.com/mapfiles/api-3/images/tmapctrl.png" draggable="false" style="position: absolute; left: 0px; top: 0px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none; width: 120px; height: 54px;"></div></div><div style="position: relative; overflow: hidden; width: 67%; height: 1px; left: 16%; background-color: rgb(230, 230, 230);"></div><div title="Zoom out" style="position: relative;"><div style="overflow: hidden; position: absolute;"><img src="http://maps.gstatic.com/mapfiles/api-3/images/tmapctrl.png" draggable="false" style="position: absolute; left: 0px; top: 0px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none; width: 120px; height: 54px;"></div></div></div></div><div class="gm-svpc" controlwidth="28" controlheight="28" style="background-color: rgb(255, 255, 255); box-shadow: rgba(0, 0, 0, 0.298039) 0px 1px 4px -1px; border-radius: 2px; width: 28px; height: 28px; cursor: url(&quot;http://maps.gstatic.com/mapfiles/openhand_8_8.cur&quot;) 8 8, default; display: none; position: absolute;"><div style="position: absolute; left: 1px; top: 1px;"></div></div><div class="gmnoprint" controlwidth="28" controlheight="0" style="display: none; position: absolute;"><div title="Rotate map 90 degrees" style="width: 28px; height: 28px; overflow: hidden; position: absolute; border-radius: 2px; box-shadow: rgba(0, 0, 0, 0.298039) 0px 1px 4px -1px; cursor: pointer; background-color: rgb(255, 255, 255); display: none;"><img src="http://maps.gstatic.com/mapfiles/api-3/images/tmapctrl4.png" draggable="false" style="position: absolute; left: -141px; top: 6px; width: 170px; height: 54px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div><div class="gm-tilt" style="width: 0px; height: 0px; overflow: hidden; position: absolute; border-radius: 2px; box-shadow: rgba(0, 0, 0, 0.298039) 0px 1px 4px -1px; top: 0px; cursor: pointer; background-color: rgb(255, 255, 255);"><img src="http://maps.gstatic.com/mapfiles/api-3/images/tmapctrl4.png" draggable="false" style="position: absolute; left: 0px; top: 0px; width: 170px; height: 54px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div></div></div><div class="gmnoprint" style="margin: 10px; z-index: 0; position: absolute; cursor: pointer; display: none; left: 0px; top: 0px;"><div class="gm-style-mtc" style="float: left;"><div draggable="false" title="Show street map" style="direction: ltr; overflow: hidden; text-align: center; position: relative; color: rgb(0, 0, 0); font-family: Roboto, Arial, sans-serif; -webkit-user-select: none; font-size: 11px; background-color: rgb(255, 255, 255); padding: 8px; border-bottom-left-radius: 2px; border-top-left-radius: 2px; -webkit-background-clip: padding-box; background-clip: padding-box; box-shadow: rgba(0, 0, 0, 0.298039) 0px 1px 4px -1px; min-width: 22px; font-weight: 500;">Map</div><div style="background-color: white; z-index: -1; padding: 2px; border-bottom-left-radius: 2px; border-bottom-right-radius: 2px; box-shadow: rgba(0, 0, 0, 0.298039) 0px 1px 4px -1px; position: absolute; left: 0px; top: 36px; text-align: left; display: none;"><div draggable="false" title="Show street map with terrain" style="color: rgb(0, 0, 0); font-family: Roboto, Arial, sans-serif; -webkit-user-select: none; font-size: 11px; background-color: rgb(255, 255, 255); padding: 6px 8px 6px 6px; direction: ltr; text-align: left; white-space: nowrap;"><span role="checkbox" style="box-sizing: border-box; position: relative; line-height: 0; font-size: 0px; margin: 0px 5px 0px 0px; display: inline-block; background-color: rgb(255, 255, 255); border: 1px solid rgb(198, 198, 198); border-radius: 1px; width: 13px; height: 13px; vertical-align: middle;"><div style="position: absolute; left: 1px; top: -2px; width: 13px; height: 11px; overflow: hidden; display: none;"><img src="http://maps.gstatic.com/mapfiles/mv/imgs8.png" draggable="false" style="position: absolute; left: -52px; top: -44px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none; width: 68px; height: 67px;"></div></span><label style="vertical-align: middle; cursor: pointer;">Terrain</label></div></div></div><div class="gm-style-mtc" style="float: left;"><div draggable="false" title="Show satellite imagery" style="direction: ltr; overflow: hidden; text-align: center; position: relative; color: rgb(86, 86, 86); font-family: Roboto, Arial, sans-serif; -webkit-user-select: none; font-size: 11px; background-color: rgb(255, 255, 255); padding: 8px; border-bottom-right-radius: 2px; border-top-right-radius: 2px; -webkit-background-clip: padding-box; background-clip: padding-box; box-shadow: rgba(0, 0, 0, 0.298039) 0px 1px 4px -1px; border-left: 0px; min-width: 40px;">Satellite</div><div style="background-color: white; z-index: -1; padding: 2px; border-bottom-left-radius: 2px; border-bottom-right-radius: 2px; box-shadow: rgba(0, 0, 0, 0.298039) 0px 1px 4px -1px; position: absolute; right: 0px; top: 36px; text-align: left; display: none;"><div draggable="false" title="Show imagery with street names" style="color: rgb(0, 0, 0); font-family: Roboto, Arial, sans-serif; -webkit-user-select: none; font-size: 11px; background-color: rgb(255, 255, 255); padding: 6px 8px 6px 6px; direction: ltr; text-align: left; white-space: nowrap;"><span role="checkbox" style="box-sizing: border-box; position: relative; line-height: 0; font-size: 0px; margin: 0px 5px 0px 0px; display: inline-block; background-color: rgb(255, 255, 255); border: 1px solid rgb(198, 198, 198); border-radius: 1px; width: 13px; height: 13px; vertical-align: middle;"><div style="position: absolute; left: 1px; top: -2px; width: 13px; height: 11px; overflow: hidden;"><img src="http://maps.gstatic.com/mapfiles/mv/imgs8.png" draggable="false" style="position: absolute; left: -52px; top: -44px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none; width: 68px; height: 67px;"></div></span><label style="vertical-align: middle; cursor: pointer;">Labels</label></div></div></div></div></div></div></div>
									<h4>ติดต่อสำนักงานตัวแทน</h4>
									<ul class="tg-contactinfo">
										<li>
											<i class="fa fa-home"></i>
											<address>79/132 THE PLANT CITY ซ.นวมินทร์ 86 ถ.นวมินทร์ แขวงรามอินทรา เขตคันนายาว กรุงเทพฯ 10230</address>
										</li>
										<li>
											<i class="fa fa-envelope-o"></i>
											<span>kt-axa@puthanavit1r.com</span>
										</li>
										
										<li>
											<i class="fa fa-phone"></i>
											<span>02-004-9500, 02-004-9400</span>
										</li>
									</ul>
								</div>
							</div>
							<div class="col-md-3 col-sm-6 col-xs-6">
								<div class="tg-widget tg-widgetusefulllinks">
									<div class="tg-widgettitle"><h3>ประกันสุขภาพ</h3></div>
									<ul>
										<li><a href="#">ไอชายด์ เฮลท์ โพรเทคชั่น</a></li>
										<li><a href="#">เพอร์เฟคเฮลท์ โซลูชั่น</a></li>
										<li><a href="#">ประกันสุขภาพเด็ก อายุ 0-15 ปี</a></li>
										<li><a href="#">ประกันสุขภาพ สุขภาพดี 6200</a></li>
											<li><a href="#">ประกันสุขภาพ วัยทำงาน</a></li>
												<li><a href="#">ประกันสุขภาพ แบบออม 12 ปีี</a></li>
									<!-- 	
									
										<li><a href="#">ประกันสุขภาพ แบบออม 20 ปี</a></li>
										<li><a href="#">ประกันโรคร้ายแรง i-Care</a></li>-->
								
									</ul>
								</div>
							</div>
			
							<div class="col-md-3 col-sm-6 col-xs-6">
								<div class="tg-widget tg-widgetusefulllinks">
									<div class="tg-widgettitle"><h3>ประกันออมทรัพย์</h3></div>
									<ul>
										<li><a href="#">ประกันทุนการศึกษาบุตร iGrow</a></li>
										<li><a href="#">ประกันออมทรัพย์ iPlus</a></li>
										<li><a href="#">ประกันออมทรัพย์ 20SS</a></li>
						
									</ul>
									<div class="tg-widgettitle"><h3>ประกันคุ้มครองรายได้</h3></div>
									<ul>
										<li><a href="#">ประกันคุ้มครองรายได้ | iProtect</a></li>
										<li><a href="#">ประกันคุ้มครองชีวิตและอุบัติเหตุ | iFine</a></li>
								
						
								
									</ul>
								</div>
							</div>
						<div class="col-md-3 col-sm-6 col-xs-6">
								<div class="tg-widget tg-widgetusefulllinks">
									<div class="tg-widgettitle"><h3>ประกันแบบบำนาญ</h3></div>
									<ul>
										<li><a href="#">ประกันบำนาญ i-Wish 10</a></li>
										<li><a href="#">ประกันบำนาญ i-Wish 55</a></li>
										<li><a href="#">ประกันบำนาญ i-Wish 60</a></li>
										<li><a href="#">ประกันบำนาญ i-Wish 65</a></li>
							
								
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="tg-footerbottombar">
				<div class="container">
					<div class="row">
						<div class="col-sm-12 col-xs-12">
							<span class="tg-copyright">ลิขสิทธิ์ถูกต้อง © 2016 www.deeagent.com เป็น website ของตัวแทนประกันชีวิต บริษัท กรุงไทย-แอกซ่า ประกันชีวิต จำกัด(มหาชน) สำนักงานหน่วย Puthanavit 1 เพื่อนำเสนอข้อมูลแบบประกันให้กับผู้ที่สนใจ สำหรับรายละเอียดสาระสำคัญต้องยึดตามที่ระบุในเอกสารของบริษัทเท่านั้น</span>
							<ul class="tg-socialicons">
								<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
								<li><a href="#"><i class="fa fa-twitter"></i></a></li>
								<li><a href="#"><i class="fa fa-facebook-f"></i></a></li>
								<li><a href="#"><i class="fa fa-pinterest-p"></i></a></li>
								<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
							</ul>
							<strong class="tg-logo"><a href="#"><img src="images/deeagentw.svg" alt="image description"></a></strong>
						</div>
					</div>
				</div>
			</div>
		</footer>
		
			<script src="js/vendor/jquery-library.js"></script>
	<script src="js/vendor/bootstrap.min.js"></script>
	<script src="http://maps.google.com/maps/api/js?key=AIzaSyCR-KEWAVCn52mSdeVeTqZjtqbmVJyfSus&language=en"></script>
	<script src="js/owl.carousel.min.js"></script>
	<script src="js/jquery.svgInject.js"></script>
	<script src="js/isotope.pkgd.js"></script>
	<script src="js/chartsloader.js"></script>
	<script src="js/parallax.js"></script>
	<script src="js/countTo.js"></script>
	<script src="js/appear.js"></script>
	<script src="js/gmap3.js"></script>
	<script src="js/main.js"></script>