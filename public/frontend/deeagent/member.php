<?php include('header.php'); ?>

	<div id="tg-innerbanner" class="tg-innerbanner tg-haslayout">
			<div class="container">
				<div class="row">
					<div class="col-sm-12">
					</br></br></br>
					</div>
				</div>
			</div>
		</div>	
	<section class="tg-main-section tg-haslayout">
	
			<div class="container">
					<div class="row">
						<div id="tg-twocolumns" class="tg-twocolumns">			
						<div class="col-lg-7 col-sm-7 col-xs-12 pull-right">
							<div class="tg-sectionhead">
								<div class="tg-sectiontitle">
									<h2>สมัครตัวแทนกรุงไทย-แอกซ่า ประกันชีวิต</h2>
										<h3 style="text-align: center;">สนใจรับข้อมูลเพิ่มเติม</h3>
								</div>
							
							</div>
						<div class="tg-widgetcontent">
												<div class="tg-sectiontitle">
							<h2>สมัครตัวแทน</h2>
									</div>	
													<form class="tg-themeform">
														<fieldset>
							
						
															<div class="form-group">
																<input type="text" class="form-control" name="" placeholder="ชื่อ-นามสกุล">
															</div>
															<div class="form-group">
																<input type="text" class="form-control" name="" placeholder="เบอร์โทรศัพท์">
															</div>
															<div class="form-group">
																<input type="text" class="form-control" name="" placeholder="อีเมล">
															</div>
																<div class="form-group">
																<input type="text-area" class="form-control" name="" placeholder="ที่อยู่">
															</div>
															
															<button class="tg-btn" type="submit"><span>สมัครเลย</span></button>
														</fieldset>
													</form>
												</br>	
												</br>	<div class="tg-widgetcontent">
						
					
					</div>
				</div>
				</div>
			
			
			
			
				<div class="col-lg-5 col-sm-5 col-xs-12 pull-left">
								<aside id="tg-sidebar" class="tg-siderbar">
								<ul>
													<li>
														<span class="tg-whatwedoicon">
															<i class="fa fa-bell-o"></i>
														</span>
														<div class="tg-whatwedocontent">
															<h4><a href="#">ชาย-หญิง อายุ 25-45 ปี</a></h4>
															<div class="tg-description">
													
ไม่จำกัดวุฒิการศึกษา </br>
ใช้คอมพิวเตอร์โปรแกรมพื้นฐานได้ดี</br>
มีบุคลิกภาพที่ดี ทัศนคติที่ดี</br>
สุจริต รักงานบริการ</br>
จัดสรรเวลาเรียน และพัฒนาตัวเอง</br>
ไม่เคยเป็นบุคลล้มละลาย</br>
มีเวลาเข้ารับการฝึกอบรมความรู้เรื่องประกันชีวิต ประกันสุขภาพ อุบัติเหตุ</br>
หากมีประสบการณ์จะได้รับการพิการณาเป็นพิเศษ*	</br>
* รับจำนวนจำกัด ปิดรับสมัครคัดเลือก 31 พฤษภาคม 2559	</br></div>
														</div>
													</li>
													<li>
														<span class="tg-whatwedoicon">
															<i class="fa fa-comments-o"></i>
														</span>
														<div class="tg-whatwedocontent">
															<h4><a href="#">มีระบบสนับสนุนที่ดี</a></h4>
															<div class="tg-description">
																
• ทำงานช่วยเหลือกันเป็นทีม </br>
• มีฐานลูกค้าให้ตาม </br>
• มีบูธงาน Event </br>
• มีการสอนทำการตลาด ฝึกอบรม </br>
• ได้รับโปรโมทสู่ผู้บริหาร </br></div>
														</div>
													</li>
													<li>
														<span class="tg-whatwedoicon">
															<i class="fa fa-flag-o"></i>
														</span>
														<div class="tg-whatwedocontent">
															<h4><a href="#">รายได้ดี</a></h4>
															<div class="tg-description">
															
• คอมมิชชั่น เดือนละ 2 ครั้ง</br>
• โบนัส ทุก 3 เดือน</br>
• โบนัสรายปี</br>
• โบนัสความยั่งยืนและต่ออายุ</br>
• ค่าบริหารทีมงานตามตำแหน่ง</br>	</div>
														</div>
													</li>
													<li>
														<span class="tg-whatwedoicon">
															<i class="fa fa-flag-o"></i>
														</span>
														<div class="tg-whatwedocontent">
															<h4><a href="#">สวัสดิการเพิ่มเติม</a></h4>
															<div class="tg-description">
															

• รักษาพยาบาล และประกันชีวิต</br>
• รางวัลท่องเที่ยวต่างประเทศ</br>
• งานมอบรางวัล งานเลี้ยง คุณวุฒิ</br>
• รางวัลพิเศษอื่นๆ</br>
</div>
														</div>
													</li>
													<li>
														<span class="tg-whatwedoicon">
															<i class="fa fa-flag-o"></i>
														</span>
														<div class="tg-whatwedocontent">
															<h4><a href="#">เอกสารสมัครตัวแทน</a></h4>
															<div class="tg-description">
															

• รูปถ่าย 1 นิ้ว 6 ใบ</br>
• สำเนาบัตรประชาชน 2 ชุด</br>
• สำเนาทะเบียนบ้าน 2 ชุด</br>
• สำเนาสมุดบัญชีเงินฝากหน้าแรกของธนาคารกรุงไทย 1 ชุด</br>
• ผู้ค้ำประกัน : สำเนาบัตรประชาชน 2 ชุด สำเนาทะเบียนบ้าน 2 ชุด สำเนาสลิปเงินเดือน</br>
• สำเนาใบเปลี่ยนชื่อ หรือ นามสกุล</br>
</div>
														</div>
													</li>
												</ul>
					</div>	</div>	</div>
			</section>

	<?php include('footernoform.php'); ?>
