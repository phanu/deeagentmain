<?php include('header.php'); ?>
<div id="tg-innerbanner" class="tg-innerbanner tg-haslayout">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<div class="tg-innerbannercontent">
					<div class="tg-pagetitle">
						<h1>ตัวแทนกรุงไทย-แอกซ่า ประกันชีวิต</h1>
					</div>
					
				</div>
			</div>
		</div>
	</div>
</div>		
<main id="tg-main" class="tg-main tg-haslayout">
	<div class="tg-pagecontent">
		<div class="container">
			<div class="row">
				<div class="tg-member tg-detailpage">
					<div class="col-sm-4 col-xs-12 pull-left">
						<figure>
						<img src="images/team/img-14.jpg" alt="image description"></figure>
					</div>
					<div class="col-sm-8 col-xs-12 pull-right">
						<div class="tg-sectionhead">
							<div class="tg-sectiontitle">
								<h2>Phansakran Dhuramonawong</h2>
								<h3>ตัวแทนกรุงไทย-แอกซ่าประกันชีวิต</h3>
							</div>
							
						</div>
						<div class="tg-memberinfo">
							<span>ชื่อ :</span>
							<span>พรรษากาล</span>
							<span>ใบอนุญาตเลขที่ :</span>
							<span>DA86</span>
							<span>พื้นที่ :</span>
							<span>นวลจันทร์</span>
							<span>เบอร์โทร :</span>
							<span>098 252 8986</span>
							<span>Line ID :</span>
							<span>@graphicbuffet</span>
							<span>Email :</span>
							<span><a href="#">info@graphicbuffet.co.th</a></span>
							<span>Social Networks :</span>
							<span>
								<ul class="tg-socialicons">
									<li><a href="#"><i class="fa fa-facebook-f"></i></a></li>
									<li><a href="#"><i class="fa fa-twitter"></i></a></li>
									<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
									<li><a href="#"><i class="fa fa-dribbble"></i></a></li>
								</ul>
							</span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</main>
<?php include('footerb.php'); ?>