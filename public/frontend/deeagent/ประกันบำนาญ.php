<?php include('header.php'); ?>
<body>

				
		<div id="tg-innerbanner" class="tg-innerbanner tg-haslayout">
			<div class="container">
				<div class="row">
					<div class="col-sm-12">
						<div class="tg-innerbannercontent">
							<div class="tg-pagetitle">
										<h3>ประกันชีวิตแบบบำนาญ</h3>
							</div>
							<ol class="tg-breadcrumb">
								<li><a href="index.php"><i class="fa fa-home"></i></a></li>
								<li><a href="/ประกันบำนาญ.php"><class="tg-active">ประกันชีวิตแบบบำนาญ</li>
							</ol>
						</div>
					</div>
				</div>
			</div>
	
		<!--************************************
				Inner Banner End
		*************************************-->
		<!--************************************
				Main Start
		*************************************-->

		<main id="tg-main" class="tg-main tg-haslayout">
			<div class="tg-pagecontent">
				<div class="container">
					<div class="row">
						<div class="col-lg-offset-2 col-lg-8 col-md-offset-1 col-md-10 col-sm-offset-0 col-sm-12 col-xs-12">
							<div class="tg-sectionhead">
								<div class="tg-sectiontitle">
									<h2>กรุงไทย-แอกซ่า ประกันชีวิต</h2>
									<h3>i-Wish ประกันบำนาญ</h3>
									
									
								</div>
								<div class="tg-description">
									<p> แผนการเกษียณอายุที่คลอบคลุมรอบด้าน
เพื่อสร้างความมั่งคั่งทางการเงินและทำให้คุณอุ่นใจด้วยความคุ้มครองสุขภาพไปพร้อมๆ กัน ให้คุณใช้ชีวิตในวัยเกษียณดั่งใจฝัน ไม่ว่าจะเป็นการท่องเที่ยวไปทั่วโลก การพักผ่อนหย่อนใจ หรือการใช้ชีวิตแบบสบายๆ ที่ห้อมล้อมไปด้วยคนที่คุณรัก เราสามารถช่วยคุณวางแผนเพื่อบรรลุเป้าหมายวัยเกษียณที่คุณต้องการได้
</p> 						
								
								</div>
							</div>
						</div>
						<div id="tg-twocolumns" class="tg-twocolumns">
	
								<div class="row">
									<div class="col-lg-9 col-sm-7 col-xs-12">
										<div id="tg-content" class="tg-content">
											<div class="tg-detailpage">
										
												<div class="tg-posttitle">
													<h2><a href="#">i-Wish ประกันบำนาญ</a></h2>
												</div>
												<div class="tg-description">
						<div class="tg-videobox tg-alignright"><iframe width="560" height="315" src="https://www.youtube.com/embed/-QLHyTVg2Nw" frameborder="0" allowfullscreen></iframe></div>
													<h3><a href="#">กรุงไทย แอคซ่า นิยามใหม่ของประกันชีวิต</a></h3>
													<p>*ผลประโยชน์ภาษีต่อภาษีต่อเบี้ยประกันภัยบำนาญในตาราง เป็นจำนวนผลประโยชน์ทางภาษีสูงสุดที่จะได้รับจากเบี้ยประกัน i-Wish คำนวนโดยใช้อัตราฐานภาษีสูงสุดในแต่ละฐานรายได้ คูณกับเบี้ยประกันภัย i-Wish ทั้งนี้ผลประโยชน์ทางภาษีที่แท้จริงจะขึ้นอยู่กับสถานะของแต่ละบุคคลรวมถึงค่าลดหย่อนภาษีอื่นๆ ที่มีอยู่ กรุณาศึกษา อ่าน และทำความเข้าใจเอกสารเพิ่มเติมของบรษัท ท่านสามารถขอข้อมูลเพิ่มเติมได้ที่สำนักงานตัวแทนโทร 02-004-9500</p>
												</div>
											</div>
											
											<div class="tg-grapharea">
												<h3>แผนประกันชีวิตแบบบำนาญ i-Wish 10</h3>
												<div id="tg-performancechart" class="tg-performancechart"></div>
											</div>
											
										</div>
									</div>
									<div class="col-lg-3 col-sm-5 col-xs-12">
										<aside id="tg-siderbar" class="tg-siderbar">
											<div class="tg-widget tg-widgetprojectdescription">
								<div class="tg-widgettitle"><h3>รายละเอียด</h3></div>
												<div class="tg-widgetcontent">
													<ul>
														<li>
															<span><em>ชำระเบี้ยประกัน</em></span>
															<span>10 ปิ</span>
														</li>
														<li>
															<span><em>เริ่มรับบำนาญ</em></span>
															<span>อายุ 60 ปี</span>
														</li>
												
														<li>
															<span>อายุรับประกันเริ่มต้น 20-50</span>
														</li>
											
													</ul>
												</div>
											</div>
											<div class="tg-widget tg-widgetbrochure">
												<div class="tg-widgettitle"><h3>ดาวน์โหลดเอกสาร</h3></div>
												<div class="tg-widgetcontent">
													<div class="tg-brochure">
														<i class="fa fa-file-pdf-o"></i>
														<a class="tg-btndownload" href="images/iwish.pdf">โบร์ชัวร์ iwish</a>
													</div>
													
												</div>
											</div>
											
										</aside>
									</div>
								</div>
							</div>
							

	
					</div>
				</div>
			</div>
		</main>
			

	<?php include('footer.php'); ?>