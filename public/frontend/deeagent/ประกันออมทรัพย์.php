<?php include('header.php'); ?>
<body>

	<div id="tg-innerbanner" class="tg-innerbanner tg-haslayout">
			<div class="tg-parallaximg" data-appear-top-offset="600" data-parallax="scroll" data-image-src="images/parallax/bgparallax-03.jpg"></div>
			<div class="container">
				<div class="row">
					<div class="col-sm-12">
						<div class="tg-innerbannercontent">
							<div class="tg-pagetitle">
										<h3>ประกันออมทรัพย์</h3>
							</div>
							<ol class="tg-breadcrumb">
								<li><a href="index.php"><i class="fa fa-home"></i></a></li>
								<li><a href="/ประกันออมทรัพย์.php"><class="tg-active">ประกันออมทรัพย์</li>
							</ol>
						</div>
					</div>
				</div>
			</div>
	
		<!--************************************
				Inner Banner End
		*************************************-->
		<!--************************************
				Main Start
		*************************************-->
		<main id="tg-main" class="tg-main tg-haslayout">
			<div class="tg-pagecontent">
				<div class="container">
					<div class="row">
						<div class="col-lg-offset-2 col-lg-8 col-md-offset-1 col-md-10 col-sm-offset-0 col-sm-12 col-xs-12">
							<div class="tg-sectionhead">
								<div class="tg-sectiontitle">
									<h2>กรุงไทย-แอกซ่า ประกันชีวิต</h2>
									<h3>ประกันออมทรัพย์</h3>
								</div>
								<div class="tg-description">
									<p>กรุงไทย-แอกซ่า ประกันชีวิต มุ่งมั่นที่จะช่วยคุณวางแผนการเกษียณอายุที่คลอบคลุมรอบด้าน เพื่อสร้างความมั่งคั่งทางการเงินและทำให้คุณอุ่นใจด้วยความคุ้มครองสุขภาพไปพร้อมๆ กัน ให้คุณใช้ชีวิตในวัยเกษียณดั่งใจฝัน ไม่ว่าจะเป็นการท่องเที่ยวไปทั่วโลก การพักผ่อนหย่อนใจ หรือการใช้ชีวิตแบบสบายๆ ที่ห้อมล้อมไปด้วยคนที่คุณรัก เราสามารถช่วยคุณวางแผนเพื่อบรรลุเป้าหมายวัยเกษียณที่คุณต้องการได้</p>
								</div>
							</div>
						</div>
						<div class="tg-events">
							
							<div class="col-md-4 col-sm-4 col-xs-12">
								<div class="tg-event">
									<figure>
										<a href="#"><img src="images/ประกันทุนการศึกษาบุตรIGROW/img-01.jpg" alt="image description"></a>
										<figcaption>
											<ul class="tg-eventmetadata">
										<li><a href="#">คุ้มครองการเสียชีวิตทุกกรณี สูงสุด 300,000บาท</a></li>
												<li><a href="#">กรณีเสียชีวิต* และเพิ่มเป็น 2 เท่า</a></li>
											</ul>
										</figcaption>
									</figure>
									<div class="tg-eventcontent tg-borderstyle">
										<div class="tg-eventtitle">
											<h2><a href="#">ประกันทุนการศึกษาบุตร IGROW</a></h2>
										</div>
										<div class="tg-description">
											<p>ประกันไอไฟน์ iFine สบายๆ ให้ความคุ้มครองชีวิตและอุบัติเหตุแบบประกันที่ให้ความคุ้มครองชีวิตและอุบัติเหตุในกรมธรรม์เดียว ครอบคลุมอุบัติเหตุหลากหลายอย่างคุ้มค่า 
										</p>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-sm-4 col-xs-12">
								<div class="tg-event">
									<figure>
										<a href="#"><img src="images/ประกันออมทรัพย์IPUS/img-01.jpg" alt="image description"></a>
										<figcaption>
											<ul class="tg-eventmetadata">
												<li><a href="#">มั่นใจกับอนาคตที่มั่นคง รับเงินครบกำหนดสัญญา 115%</a></li>
												<li><a href="#">ความคุ้มครองที่สูง สะสมเพื่อเป็นกองทุนมรดก</a></li>
											</ul>
										</figcaption>
									</figure>
									<div class="tg-eventcontent tg-borderstyle">
										<div class="tg-eventtitle">
											<h2><a href="#">ประกันออมทรัพย์ IPLUS</a></h2>
										</div>
										<div class="tg-description">
											<p>จะดีกว่าไหมหากเราสามารถเปลี่ยนค่าใช้จ่ายให้เป็นเงินออมเพื่ออนาคตของคุณและครอบครัว เริ่มวางแผนและรับตารางผลประโยชน์และความคุ้มครองประกอบการตัดสินใจได้ที่นี่</p>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-sm-4 col-xs-12">
								<div class="tg-event">
									<figure>
										<a href="#"><img src="images/ประกันออมทรัพย์20SS/img-01.jpg" alt="image description"></a>
										<figcaption>
											<ul class="tg-eventmetadata">
												<li><a href="#">June 27, 2016</a></li>
												<li><a href="#">09:00 am</a></li>
												<li><a href="#">Manchester, UK</a></li>
											</ul>
										</figcaption>
									</figure>
									<div class="tg-eventcontent tg-borderstyle">
										<div class="tg-eventtitle">
											<h2><a href="#">ประกันออมทรัพย์ 20SS</a></h2>
										</div>
										<div class="tg-description">
											<p>Consectetur adipisicing elit sedo eiusmod tempor dunt ut labore et dolore magna aliqua.</p>
										</div>
									</div>
								</div>
							</div>
					
	
					</div>
				</div>
			</div>
		</main>
			

	<?php include('footer.php'); ?>