<!doctype html>
<!--[if lt IE 7]>		<html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>			<html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>			<html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->	<html class="no-js" lang=""> <!--<![endif]-->
<head>
	 <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <meta name="keywords" content="ประกันสุขภาพ, ประกันบำนาญ, ประกันสะสมทรัพย์, ประกันชีวิต, ประกันโรคร้ายแรง, กรุงไทย-แอกซ่า" />
  <meta name="description" content="ประกันสุขภาพ ประกันบำนาญ ประกันสะสมทรัพย์ แบบตลอดชีพ ประกันสุขภาพเหมาจ่าย รายละเอียดข้อมูลแผนประกัน บริษัท กรุงไทย-แอกซ่า ประกันชีวิต" />
  <meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>กรุงไทย-แอกซ่า ประกันชีวิต</title>
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="apple-touch-icon" href="apple-touch-icon.png">
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/normalize.css">
	<link rel="stylesheet" href="css/font-awesome.min.css">
	<link rel="stylesheet" href="css/icomoon.css">
	<link rel="stylesheet" href="css/transitions.css">
	<link rel="stylesheet" href="css/owl.carousel.css">
	<link rel="stylesheet" href="css/owl.theme.css">
	<link rel="stylesheet" href="css/fontface.css">
	<link rel="stylesheet" href="css/main.css">
	<link rel="stylesheet" href="css/color.css">
	<link rel="stylesheet" href="css/responsive.css">
	<script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
</head>
<body class="tg-home tg-homeversion">
	<!--[if lt IE 8]>
		<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
	<![endif]-->
	<!--************************************
			Wrapper Start
	*************************************-->
	<div id="tg-wrapper" class="tg-wrapper tg-haslayout">
		<!--************************************
				Header Start
		*************************************-->
		<header id="tg-header" class="tg-header tg-haslayout">
			<div class="tg-topbar tg-bglight tg-haslayout">
				<div class="container">
					<div class="row">
						<div class="col-sm-12 col-xs-12">
							<ul class="tg-topcontactinfo">
								
								<li>
										<span style="color: #00549c;">เว็บไซต์ตัวแทน กรุงไทย-แอกซ่าประกันชีวิต</span>
								</li>
								
							</ul>
							<nav class="tg-addnav tg-themecolor">
							<ul>
									<li><a href="http://deeagent.com/contactus.php">ติดต่อเรา</a></li>
								<li><a href="http://deeagent.com/aboutus.php">เกี่ยวกับเรา</a></li>
									<li><a href="http://deeagent.com/member.php">สมัครตัวแทน</a></li>
							</ul>
						</nav>	
						</div>
					</div>
				</div>
			</div>
			<div class="tg-navigationarea">
				<div class="container">
					<div class="row">
						<div class="col-xs-12">
							<strong class="tg-logo">
								<a href="index.html"><img src="images/deeagent.svg" alt="image description"></a>
							</strong>
							<nav id="tg-nav" class="tg-nav">
								<div class="navbar-header">
									<button type="button" class="tg-btnnav navbar-toggle collapsed" data-toggle="collapse" data-target="#tg-navigation" aria-expanded="false">
										<span class="sr-only">Toggle navigation</span>
										<span class="icon-bar"></span>
										<span class="icon-bar"></span>
										<span class="icon-bar"></span>
									</button>
								</div>
								<div id="tg-navigation" class="collapse navbar-collapse tg-navigation">
									<ul>
								
								<li class="tg-hasdropdown">
												<a href="index.php">หน้าแรก</a>
											<ul>	
												<li><a href="member/agentb.php">ตัวแทน</a></li>
							
											</ul>
										</li>
										<li class="tg-hasdropdown">
											<a href="ประกันสุขภาพ.php">ประกันสุขภาพ</a>
											<ul>	
										<li><a href="#">ไอชายด์ เฮลท์ โพรเทคชั่น</a></li>
										<li><a href="#">เพอร์เฟคเฮลท์ โซลูชั่น</a></li>
										<li><a href="#">ประกันสุขภาพเด็ก อายุ 0-15 ปี</a></li>
										<li><a href="#">ประกันสุขภาพ สุขภาพดี 6200</a></li>
										<li><a href="#">ประกันสุขภาพ วัยทำงาน</a></li>
										<li><a href="#">ประกันสุขภาพ แบบออม 12 ปีี</a></li>
										<li><a href="#">ประกันสุขภาพ แบบออม 20 ปี</a></li>
										<li><a href="#">ประกันโรคร้ายแรง i-Care</a></li>
							
											</ul>
										</li>
										
											<li class="tg-hasdropdown">
											<a href="ประกันคุ้มครองรายได้.php">ประกันคุ้มครองรายได้</a>
											
											<ul>		
													<li><a href="12pl.php">ประกันตลอดชีพ 12PL สมาร์ทโปร</a></li>
													<li><a href="ประกันคุ้มครองรายได้.php">ประกันคุ้มครองรายได้ iProtect</a></li>
										<li><a href="ประกันคุ้มครองรายได้.php">ประกันชีวิตและอุบัติเหตุ iFine</a></li>

								
										
								
							
											</ul>
										</li>
										<li class="tg-hasdropdown">
											<a href="ประกันบำนาญ.php">ประกันบำนาญ i-Wish</a>
										
										</li>
										<li class="tg-hasdropdown">
											<a href="ประกันออมทรัพย์.php">ประกันออมทรัพย์</a>
											<ul>
											<li><a href="">ประกันทุนการศึกษาบุตร iGrow</a></li>
										<li><a href="#">ประกันออมทรัพย์ iPlus</a></li>
										<li><a href="#">ประกันออมทรัพย์ 20SS</a></li>
											</ul>
										</li>
								
									</ul>
								</div>
							</nav>
						</div>
					</div>
				</div>
			</div>
		</header>