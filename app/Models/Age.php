<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class age extends Sximo  {
	
	protected $table = 'age';
	protected $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT age.* FROM age  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE age.id IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
