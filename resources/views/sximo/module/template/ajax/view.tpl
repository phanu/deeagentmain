@if($setting['view-method'] =='native')
<div class="box box-primary">
	<div class="box-header with-border">
		
		<div class="box-header-tools pull-left" >
			@if($access['is_add'] ==1)
	   		<a href="{{ url('{class}/update/'.$id.'?return='.$return) }}" class="tips btn btn-default btn-xs " title="{{ Lang::get('core.btn_edit') }}" onclick="ajaxViewDetail('#{class}',this.href); return false; "><i class="fa  fa-pencil"></i></a>
			@endif 
			<a href="{{ url('{class}/show/'.$id.'?&print=true&return='.$return) }}" class="tips btn btn-xs btn-default" title="{{ Lang::get('core.btn_print') }}" onclick="ajaxPopupStatic(this.href); return false;"><i class="fa  fa-print"></i></a>			
					
		</div>	

		<div class="box-header-tools pull-right " >
			<a href="{{ ($prevnext['prev'] != '' ? url('{class}/show/'.$prevnext['prev'].'?return='.$return ) : '#') }}" class="tips btn btn-default btn-xs " @if($prevnext['prev'] != '') onclick="ajaxViewDetail('#{class}',this.href); return false; " @endif ><i class="fa fa-arrow-left"></i>  </a>	
			<a href="{{ ($prevnext['next'] != '' ? url('{class}/show/'.$prevnext['next'].'?return='.$return ) : '#') }}" class="tips btn btn-default btn-xs" @if($prevnext['next'] != '') onclick="ajaxViewDetail('#{class}',this.href); return false; " @endif> <i class="fa fa-arrow-right"></i>  </a>
			<a href="javascript:void(0)" class="collapse-close tips btn btn-default btn-xs" onclick="ajaxViewClose('#{{ $pageModule }}')">
			<i class="fa fa-remove"></i></a> 			
		</div>

	 </div>

	<div class="box-body"> 
@endif	

		<table class="table  table-bordered" >
			<tbody>	
				{form_view}
			</tbody>	
		</table>  
			
		 {masterdetailview}	

@if($setting['form-method'] =='native')
	</div>	
</div>	
@endif	

<script>
$(document).ready(function(){

});
</script>	