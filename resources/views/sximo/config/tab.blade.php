<?php

$tabs = array(
		'' 		        => '<i class="fa  fa-info-circle"></i> '. Lang::get('core.tab_siteinfo'),
		'email'			=> '<i class="fa fa-envelope"></i> '. Lang::get('core.tab_email'),
		'security'		=> '<i class="fa fa-lock"></i> '. Lang::get('core.tab_loginsecurity') ,
		'translation'	=>'<i class="fa fa-flag"></i> '.Lang::get('core.tab_translation'),
		'log'			=>'<i class="fa  fa-trash-o"></i> '. Lang::get('core.m_clearcache')
	);

?>

<ul class="nav nav-tabs" >
@foreach($tabs as $key=>$val)
	<li  @if($key == $active) class="active" @endif><a href="{{ URL::to('sximo/config/'.$key)}}"> {!! $val !!}  </a></li>
@endforeach

</ul>