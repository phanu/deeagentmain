@extends('layouts.app')

@section('content')
  <script type="text/javascript" src="{{ asset('sximo/js/simpleclone.js') }}"></script>
    <section class="content-header">
      <h1> <i class="fa fa-database"></i>  Database <small> Manage tables </small></h1>
      <ol class="breadcrumb">
        <li><a href="{{ url('dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li  class="active"> Database </li>
      </ol>
    </section>

  <div class="content"> 

 		<div class="ajaxLoading"></div>
<div class="box box-primary">
	<div class="box-header with-border"> <h3> Database Tables  <small> Manage Database Tables </small> </h3>
			<span class="pull-right">	
				<a href="{{ URL::TO('sximo/tables/tableconfig/')}}" class="btn btn-xs btn-default linkConfig"><i class="fa fa-plus-circle"></i> New Table </a>
				<a href="{{ URL::TO('sximo/tables/mysqleditor/')}}" class="btn btn-xs btn-default linkConfig"><i class="fa fa-pencil"></i> MySQL Editor </a>
			</span>	
			</div>
			<div class="box-body">

			

			<div class="row">
				<div class="col-md-3">
					{!! Form::open(array('url'=>'sximo/tables/tableremove/', 'class'=>'form-horizontal','id'=>'removeTable' )) !!}
					<div class="table-responsive">
						<table class="table table-striped">
							<thead>
								<tr>
									
									<th width="30"> <input type="checkbox" class="checkall i-checks-all " /></th>
									<th> Table Name </th>
									<th width="50"> Action </th>
								</tr>
							</thead>
							<tbody>
							@foreach($tables as $table)
								<tr>
									<td><input type="checkbox" class="ids  i-checks" name="id[]" value="{{ $table }}" /> </td>
									<td><a href="{{ URL::TO('sximo/tables/tableconfig/'.$table)}}" class="linkConfig" > {{ $table }}</a></td>
									<td>
									<a href="javascript:void(0)" onclick="droptable()" class="btn btn-xs btn-danger"><i class="fa fa-trash-o"></i></a>
									</td>
								</tr>
							@endforeach
							</tbody>
						
						</table>
					
					</div>
					{!! Form::close() !!}		
				</div>
				<div class="col-md-9">
					
					<div class="tableconfig" style="background:#fff; padding:10px; min-height:300px; border:solid 1px #ddd;">

					</div>

				</div>

			</div>
		</div>
		</div>
	
	
</div>	  
<script type="text/javascript">
$(document).ready(function(){

	$('.linkConfig').click(function(){
		$('.ajaxLoading').show();
		var url =  $(this).attr('href');
		$.get( url , function( data ) {
			$( ".tableconfig" ).html( data );
			$('.ajaxLoading').hide();
			
			
		});
		return false;
	});
});

function droptable()
{
	if(confirm('are you sure remove selected table(s) ?'))
	{
		$('#removeTable').submit();
	} else {
		return false;
	}
}

</script>
@endsection