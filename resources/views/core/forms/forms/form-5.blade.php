<section class="tg-main-section tg-haslayout">
		<div class="container">
				
	<div class="tg-widgetcontent">
						<div class="tg-sectiontitle">
							<h2>รับรายละเอียด เบี้ยประกัน</h2>
						</div>	
					

{!! Form::open(array('url'=>'home/proccess/5', 'id'=>'formconfiguration','class'=>'tg-themeform' ,'files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}
@if(Session::has('message'))	  
		{!! Session::get('message') !!}
@endif	
<fieldset>
<ul class="parsley-error-list">
	@foreach($errors->all() as $error)
		<li>{{ $error }}</li>
	@endforeach
</ul>


					<div class="form-group">
					<div class="tg-select">
					<?php 
					$partners_opt = array( '' => 'เลือกแบบประกัน' ,  'ประกันสุขภาพเด็ก 0 -15 ปี' => 'ประกันสุขภาพเด็ก 0 -15 ปี' ,  'ประกันสุขภาพ' => 'ประกันสุขภาพ' ,  'ประกันสะสมทรัพย์' => 'ประกันสะสมทรัพย์' ,  'ประกันบำนาญ' => 'ประกันบำนาญ' ,  'ประกันคุ้มครองรายได้ หรือมรดก' => 'ประกันคุ้มครองรายได้ หรือมรดก' , ); ?>
					<select name='partners' rows='5' required  class='form-control '  > 
						<?php 
						foreach($partners_opt as $key=>$val)
						{
							echo "<option  value ='$key' >$val</option>"; 						
						}						
						?></select>
						</div>
		</div>

		<div class="form-group  " >
				<div class="tg-select"><select name='age' rows='5' id='age' class='form-control ' required  ></select></div>
		</div>

		<div class="form-group  " >
				<div class="tg-select"><select name='province' rows='5' id='province' class='form-control ' required  ></select></div>
		</div>

		<div class="form-group  " >
				{!! Form::text('fullname','',array('class'=>'form-control', 'placeholder'=>'ชื่อ-นามสกุล', 'required'=>'true'  )) !!}
		</div>

		<div class="form-group  " >
				{!! Form::text('phone','',array('class'=>'form-control', 'placeholder'=>'เบอร์โทรศัพท์', 'required'=>'true'  )) !!}
		</div>

		<div class="form-group  " >
				{!! Form::text('email','',array('class'=>'form-control', 'placeholder'=>'อีเมล', 'required'=>'true'  )) !!}
		</div>

		
		<div class="form-group  " >					
				<button type="submit" class="tg-btn"><span>รับเลย</span></button>
		</div>
</fieldset>
</div>
</div>
</section>
{!! Form::close() !!}

<link href="{{ asset('sximo/js/plugins/iCheck/skins/square/red.css')}}" rel="stylesheet">
<script type="text/javascript" src="{{ asset('sximo/js/plugins/iCheck/icheck.min.js') }}"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$('input[type="checkbox"],input[type="radio"]').iCheck({
			checkboxClass: 'icheckbox_square-red',
			radioClass: 'iradio_square-red',
		});	
	});

	
		$("#age").jCombo("{!! url('post/comboselect?filter=age:value:agename') !!}",
		{  selected_value : '' });
		
		$("#province").jCombo("{!! url('post/comboselect?filter=province:value:provincename') !!}",
		{  selected_value : '' });
		
</script>