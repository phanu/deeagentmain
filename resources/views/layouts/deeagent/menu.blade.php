{{--*/ $menus = SiteHelpers::menus('top') /*--}}
<div class="tg-navigationarea">
				<div class="container">
					<div class="row">
						<div class="col-xs-12">
							<strong class="tg-logo">
								<a href="{{ URL::to('dashboard')}}"><img src="http://deeagent.com/sximo/images/{{CNF_LOGO}}" alt="{{ CNF_APPNAME }}"></a>
							</strong>
							<nav id="tg-nav" class="tg-nav">
								<div class="navbar-header">
									<button type="button" class="tg-btnnav navbar-toggle collapsed" data-toggle="collapse" data-target="#tg-navigation" aria-expanded="false">
										<span class="sr-only">Toggle navigation</span>
										<span class="icon-bar"></span>
										<span class="icon-bar"></span>
										<span class="icon-bar"></span>
									</button>
								</div>
								<div id="tg-navigation" class="collapse navbar-collapse tg-navigation">
									<ul>
											@foreach ($menus as $menu)
			<li class="tg-hasdropdown">
			 	<a 
				@if($menu['menu_type'] =='external')
					href="{{ URL::to($menu['url'])}}" 
				@else
					href="{{ URL::to($menu['module'])}}" 
				@endif
			 
				>
					@if(CNF_MULTILANG ==1 && isset($menu['menu_lang']['title'][Session::get('lang')]) && $menu['menu_lang']['title'][Session::get('lang')]!='')
						{{ $menu['menu_lang']['title'][Session::get('lang')] }}
					@else
						{{$menu['menu_name']}}
					@endif	
				
				</a> 

				@if(count($menu['childs']) > 0)
					 <ul>
						@foreach ($menu['childs'] as $menu2)
						 <li>
						 	<a 
								@if($menu2['menu_type'] =='external')
									href="{{ URL::to($menu2['url'])}}" 
								@else
									href="{{ URL::to($menu2['module'])}}" 
								@endif
											
							>
									@if(CNF_MULTILANG ==1 && isset($menu2['menu_lang']['title'][Session::get('lang')]))
										{{ $menu2['menu_lang']['title'][Session::get('lang')] }}
									@else
										{{$menu2['menu_name']}}
									@endif
								
							</a> 
							@if(count($menu2['childs']) > 0)
							<ul>
								@foreach($menu2['childs'] as $menu3)
									<li>
										<a 
											@if($menu3['menu_type'] =='external')
												href="{{ URL::to($menu3['url'])}}" 
											@else
												href="{{ URL::to($menu3['module'])}}" 
											@endif										
										
										>
											<span>
											@if(CNF_MULTILANG ==1 && isset($menu3['menu_lang']['title'][Session::get('lang')]))
												{{ $menu3['menu_lang']['title'][Session::get('lang')] }}
											@else
												{{$menu3['menu_name']}}
											@endif
											
											</span>  
										</a>
									</li>	
								@endforeach
							</ul>
							@endif							
							
						</li>							
						@endforeach
					</ul>
				@endif

			</li>
			@endforeach		
								
								
									</ul>
								</div>
							</nav>
						</div>
					</div>
				</div>
</div>