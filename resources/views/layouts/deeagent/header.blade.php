<!doctype html>
<!--[if lt IE 7]>		<html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>			<html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>			<html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->	<html class="no-js" lang=""> <!--<![endif]-->
<head>
	 <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <meta name="keywords" content="{{ $pageMetakey }}" />
  <meta name="description" content="{{ $pageMetadesc }}" />
  <meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>{{ CNF_APPNAME }} | {{ $pageTitle}}</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="apple-touch-icon" href="http://deeagent.com/frontend/deeagent/apple-touch-icon.png">
	<link rel="stylesheet" href="http://deeagent.com/frontend/deeagent/css/bootstrap.min.css">
	<link rel="stylesheet" href="http://deeagent.com/frontend/deeagent/css/normalize.css">
	<link rel="stylesheet" href="http://deeagent.com/frontend/deeagent/css/font-awesome.min.css">
	<link rel="stylesheet" href="http://deeagent.com/frontend/deeagent/css/icomoon.css">
	<link rel="stylesheet" href="http://deeagent.com/frontend/deeagent/css/transitions.css">
	<link rel="stylesheet" href="http://deeagent.com/frontend/deeagent/css/owl.carousel.css">
	<link rel="stylesheet" href="http://deeagent.com/frontend/deeagent/css/owl.theme.css">
	<link rel="stylesheet" href="http://deeagent.com/frontend/deeagent/css/fontface.css">
	<link rel="stylesheet" href="http://deeagent.com/frontend/deeagent/css/main.css">
	<link rel="stylesheet" href="http://deeagent.com/frontend/deeagent/css/color.css">
	<link rel="stylesheet" href="http://deeagent.com/frontend/deeagent/css/responsive.css">
	<script src="http://deeagent.com/frontend/deeagent/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>

	
</head>
<body class="tg-home tg-homeversion">
	<!--[if lt IE 8]>
		<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
	<![endif]-->
	<!--************************************
			Wrapper Start
	*************************************-->
	<div id="tg-wrapper" class="tg-wrapper tg-haslayout">
		<!--************************************
				Header Start
		*************************************-->
		<header id="tg-header" class="tg-header tg-haslayout">
			<div class="tg-topbar tg-bglight tg-haslayout">
				<div class="container">
					<div class="row">
						<div class="col-sm-12 col-xs-12">
							<ul class="tg-topcontactinfo">
								<li>
										<span style="color: #00549c;">{!! html_entity_decode(Lang::get('header.1')) !!}</span>
								</li>
							</ul>
							<nav class="tg-addnav tg-themecolor">
							<ul>
									<li><a href="{{url('ติดต่อเรา')}}">{!! html_entity_decode(Lang::get('header.2')) !!}</a></li>
									<li><a href="{{url('เกี่ยวกับเรา')}}">{!! html_entity_decode(Lang::get('header.3')) !!}</a></li>
									<li><a href="{{url('สมัครตัวแทน')}}">{!! html_entity_decode(Lang::get('header.4')) !!}</a></li>
							</ul>
							</nav>	
						</div>
					</div>
				</div>
			</div>

			@include('layouts/deeagent/menu')
		</header>

		