<!doctype html>
<!--[if lt IE 7]>		<html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>			<html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>			<html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->	<html class="no-js" lang=""> <!--<![endif]-->
<head>
	 <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <meta name="keywords" content="{{ CNF_APPNAME }}" />
  <meta name="description" content="{{ CNF_APPDESC }}" />
  <meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>{{ CNF_APPNAME }} | {{ CNF_APPDESC }}</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="apple-touch-icon" href="{{ asset('frontend') }}/deeagent/apple-touch-icon.png">
	<link rel="stylesheet" href="{{ asset('frontend') }}/deeagent/css/bootstrap.min.css">
	<link rel="stylesheet" href="{{ asset('frontend') }}/deeagent/css/normalize.css">
	<link rel="stylesheet" href="{{ asset('frontend') }}/deeagent/css/font-awesome.min.css">
	<link rel="stylesheet" href="{{ asset('frontend') }}/deeagent/css/icomoon.css">
	<link rel="stylesheet" href="{{ asset('frontend') }}/deeagent/css/transitions.css">
	<link rel="stylesheet" href="{{ asset('frontend') }}/deeagent/css/owl.carousel.css">
	<link rel="stylesheet" href="{{ asset('frontend') }}/deeagent/css/owl.theme.css">
	<link rel="stylesheet" href="{{ asset('frontend') }}/deeagent/css/fontface.css">
	<link rel="stylesheet" href="{{ asset('frontend') }}/deeagent/css/main.css">
	<link rel="stylesheet" href="{{ asset('frontend') }}/deeagent/css/color.css">
	<link rel="stylesheet" href="{{ asset('frontend') }}/deeagent/css/responsive.css">
	<script src="{{ asset('frontend') }}/deeagent/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>

	
</head>
<body class="tg-home tg-homeversion">
	<!--[if lt IE 8]>
		<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
	<![endif]-->
	<!--************************************
			Wrapper Start
	*************************************-->
	<div id="tg-wrapper" class="tg-wrapper tg-haslayout">
		<!--************************************
				Header Start
		*************************************-->
		<header id="tg-header" class="tg-header tg-haslayout">
			<div class="tg-topbar tg-bglight tg-haslayout">
				<div class="container">
					<div class="row">
						<div class="col-sm-12 col-xs-12">
							<ul class="tg-topcontactinfo">
								<li>
										<span style="color: #00549c;">{!! html_entity_decode(Lang::get('header.1')) !!}</span>
								</li>
							</ul>
							<nav class="tg-addnav tg-themecolor">
							<ul>
									<li><a href="{{url('ติดต่อเรา')}}">{!! html_entity_decode(Lang::get('header.2')) !!}</a></li>
									<li><a href="{{url('เกี่ยวกับเรา')}}">{!! html_entity_decode(Lang::get('header.3')) !!}</a></li>
									<li><a href="{{url('สมัครตัวแทน')}}">{!! html_entity_decode(Lang::get('header.4')) !!}</a></li>
							</ul>
							</nav>	
						</div>
					</div>
				</div>
			</div>

			
		</header>

	<section class="tg-main-section tg-haslayout">
	
			<div class="container">
					<div class="row">

@yield('content')
					</div>
			</div>
	</section>

	
	<script src="{{ asset('frontend') }}/deeagent/js/vendor/jquery-library.js"></script>
	<script src="{{ asset('frontend') }}/deeagent/js/vendor/bootstrap.min.js"></script>
	<script src="http://maps.google.com/maps/api/js?key=AIzaSyCR-KEWAVCn52mSdeVeTqZjtqbmVJyfSus&language=th"></script>
	<script src="{{ asset('frontend') }}/deeagent/js/owl.carousel.min.js"></script>
	<script src="{{ asset('frontend') }}/deeagent/js/jquery.svgInject.js"></script>
	<script src="{{ asset('frontend') }}/deeagent/js/isotope.pkgd.js"></script>
	<script src="{{ asset('frontend') }}/deeagent/js/chartsloader.js"></script>
	<script src="{{ asset('frontend') }}/deeagent/js/parallax.js"></script>
	<script src="{{ asset('frontend') }}/deeagent/js/countTo.js"></script>
	<script src="{{ asset('frontend') }}/deeagent/js/appear.js"></script>
	<script src="{{ asset('frontend') }}/deeagent/js/gmap3.js"></script>
	<script src="{{ asset('frontend') }}/deeagent/js/main.js"></script>
	<script type="text/javascript" src="{{ asset('sximo/js/plugins/jquery.jCombo.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('sximo/js/plugins/parsley.js') }}"></script>

</body>
</html>