 <header class="main-header">
    <!-- Logo -->
    <a href="{{ url('dashboard')}}" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini">
       
        <img src="{{ asset('sximo/images/logo.png') }}" alt="{{ CNF_APPNAME }}" height="30" width="30" />
               
      </span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg">
    
        
        @if(file_exists(public_path().'/sximo/images/'.CNF_LOGO) && CNF_LOGO !='')
        <img src="{{ asset('sximo/images/'.CNF_LOGO)}}" alt="{{ CNF_APPNAME }}"  />
        @else
        <img src="{{ asset('sximo/images/logo.png')}}" alt="{{ CNF_APPNAME }}"  />
        @endif 
      </span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <li><a href="{{ url('')}}" target="_blank"><i class="fa fa-globe"></i> </a>  </li>
          <!-- Notifications: style can be found in dropdown.less -->
          <li class="dropdown notifications-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-bell-o"></i>
              <span class="label label-warning notif-alert">0</span>
            </a>
            <ul class="dropdown-menu">
              <li class="header">You have <span class="notif-alert" style="font-weight: 600">0</span> notifications</li>
              <li>
                <!-- inner menu: contains the actual data -->
                <ul class="menu" id="notification-menu">
                  
                </ul>  
              <li class="footer"><a href="{{ url('notification')}}">View all</a></li>
            </ul>
          </li>
          <!-- Tasks: style can be found in dropdown.less -->

         @if(CNF_MULTILANG ==1)
        <li class="dropdown tasks-menu">
          <?php 
          $flag ='en';
          $langname = 'English'; 
          foreach(SiteHelpers::langOption() as $lang):
            if($lang['folder'] == Session::get('lang') or $lang['folder'] == 'CNF_LANG') {
              $flag = $lang['folder'];
              $langname = $lang['name']; 
            }
            
          endforeach;?>
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
            <img class="flag-lang" src="{{ asset('sximo/images/flags/'.$flag.'.png') }}" width="16" height="12" alt="lang" /> {{ strtoupper($flag) }}
            <span class="hidden-xs">
             <i class="fa fa-angle-down"></i>
            </span>
          </a>

           <ul class="dropdown-menu dropdown-menu-right icons-right">
            <li class="header"> {{ Lang::get('core.m_sel_lang') }} </li>
            @foreach(SiteHelpers::langOption() as $lang)
              <li><a href="{{ URL::to('home/lang/'.$lang['folder'])}}"><img class="flag-lang" src="{{ asset('sximo/images/flags/'. $lang['folder'].'.png')}}" width="16" height="11" alt="lang"  /> {{  $lang['name'] }}</a></li>
            @endforeach 
          </ul>

        </li> 
        @endif

          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
             {!! SiteHelpers::avatar( 19 ) !!}
              <span class="hidden-xs">{{ Session::get('fid')}}</span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
               {!! SiteHelpers::avatar( 40 ) !!}

                <p>
                  {{ Session::get('fid')}}
                  <small>{{ Lang::get('core.m_membersince') }} . {{ date("F j, Y", strtotime(Session::get('join'))) }}</small>
                </p>
              </li>
              <!-- Menu Body -->
              <li class="user-body">
                <div class="row">
                  <div class="col-xs-12 text-center">
                 <b> {{ Lang::get('core.lastlogin') }} : </b> <br /> {{ date("H:i F j, Y", strtotime(Session::get('ll'))) }}
                   </div> 
                </div>
<!-- search form -->
      <form action="#" method="get" class="sidebar-form">
       <?php
       $templates = array(

          'skin-blue'        => 'Blue',
          'skin-black'       => 'Black',
          'skin-purple'      => 'Purple',
          'skin-green'       => 'Green',
          'skin-red'         => 'Red',
          'skin-yellow'      => 'Yellow',
          'skin-blue-light'   => 'Blue Light',
          'skin-black-light'  => 'Black Light',
          'skin-purple-light' => 'Purple Light',
          'skin-green-light'  => 'Green Light',
          'skin-red-light'    => 'Red Light',
          'skin-yellow-light' => 'Yellow Light',


        );
       ?>
        <select class="form-control" id="skin_opt" onchange="change_skin(this.value)" >
          <option value=""> -- {{ Lang::get('core.m_sel_theme') }} --</option>
          @foreach($templates as $key=>$val)
             <option value="{{ $key }}"> {{ $val }} </option>

          @endforeach
        </select>
          
       
      </form>
      <!-- /.search form -->   
                
                <!-- /.row -->
              </li>
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <a href="{{ url('user/profile')}}" class="btn btn-info btn-flat">{{ Lang::get('core.m_profile') }}</a>
                   <a href="{{ url('core/elfinder')}}" class="btn btn-success btn-flat">{{ Lang::get('core.m_files') }} </a>
                </div>
                <div class="pull-right">
                  <a href="{{ url('user/logout')}}" class="btn btn-danger btn-flat">{{ Lang::get('core.m_logout') }}</a>
                </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
          <li>
            <a href="#" data-toggle="control-sidebar"><i class="fa fa-desktop"></i></a>
          </li>
        </ul>
      </div>
    </nav>
  </header>