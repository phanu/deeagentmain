  <?php $sidebar = SiteHelpers::menus('sidebar') ;?>
 <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
         {!! SiteHelpers::avatar( 40 ) !!} 
        </div>
        <div class="pull-left info">
          <p>  Hello , {{ Session::get('fid')}}</p>
          <a href="#"><i class="fa fa-circle text-success"></i> {{ Lang::get('core.lastlogin') }} : <br /> {{ date("H:i F j, Y", strtotime(Session::get('ll'))) }}</a>
        </div>
      </div>

   


  <ul class="sidebar-menu"> 
    @foreach ($sidebar as $menu)
       
      @if($menu['module'] =='separator')
        <li class="header"> {{$menu['menu_name']}} </li>        
      @else
          <li class="treeview @if(Request::segment(1) == $menu['module']) active @endif">
          <a 
            @if($menu['menu_type'] =='external')
              href="{{ $menu['url'] }}" 
            @else
              href="{{ URL::to($menu['module'])}}" 
            @endif
          >
            <i class="{{$menu['menu_icons']}}"></i> 
            <span>
              @if(CNF_MULTILANG ==1 && isset($menu['menu_lang']['title'][Session::get('lang')]))
                {{ $menu['menu_lang']['title'][Session::get('lang')] }}
              @else
                {{$menu['menu_name']}}
              @endif              
            </span>
            @if(count($menu['childs']) > 0 )
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
            @endif
          </a>
          <!--- LEVEL II -->
            @if(count($menu['childs']) > 0 )

              <ul class="treeview-menu">
               @foreach ($menu['childs'] as $menu2)
                <li @if(Request::segment(1) == $menu2['module']) class="active" @endif >
                  <a 
                    @if($menu2['menu_type'] =='external')
                      href="{{ $menu2['url']}}" 
                    @else
                      href="{{ url($menu2['module'])}}"  
                    @endif                  
                  >                
                  <i class="{{$menu2['menu_icons']}}"></i>
                  @if(CNF_MULTILANG ==1 && isset($menu2['menu_lang']['title'][Session::get('lang')]))
                    {{ $menu2['menu_lang']['title'][Session::get('lang')] }}
                  @else
                    {{$menu2['menu_name']}}
                  @endif
                   @if(count($menu2['childs']) > 0 )
                    <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                   @endif 
                </a>
                  <!-- LEVEL III -->

                    @if(count($menu2['childs']) > 0)
                    <ul class="treeview-menu">
                       @foreach ($menu2['childs'] as $menu3)
                            <li  @if(Request::segment(1) == $menu3['module']) class="active" @endif>
                                <a 
                                  @if($menu3['menu_type'] =='external')
                                    href="{{ $menu3['url']}}" 
                                  @else
                                    href="{{ url($menu3['module'])}}"  
                                  @endif                  
                                >                
                                <i class="{{$menu3['menu_icons']}}"></i>
                                @if(CNF_MULTILANG ==1 && isset($menu3['menu_lang']['title'][Session::get('lang')]))
                                  {{ $menu3['menu_lang']['title'][Session::get('lang')] }}
                                @else
                                  {{$menu3['menu_name']}}
                                @endif
                              </a>

                           </li> 
                        @endforeach  

                    </ul>  
                     @endif 
                  <!-- END LEVEL III -->
                </li>
                @endforeach 
              </ul>
            @endif 
            <!-- END LEVEL II -->
          </li>
          @endif 
        @endforeach  
        @if(Auth::user()->group_id == 1 or Auth::user()->group_id == 2 )
        <li class="header">ADMINISTRATOR</li>
        <li class="treeview">
          <a href="javascript:void(0)"> <i class="fa fa-desktop"></i> 
            <span> Administrator Area </span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
           <ul class="treeview-menu">

            <li @if(Request::segment(2) == 'users') class="active" @endif>
              <a href="{{ url('core/users')}}">
                <i class="fa fa-user"></i> <span> {{ Lang::get('core.m_users') }} ,  {{ Lang::get('core.m_groups') }} & Email</span>
                
              </a>  
             </li>       
            
            
            <li @if(Request::segment(2) == 'pages') class="active" @endif><a href="{{ url('core/pages')}}"><i class="fa fa-files-o "></i> <span>{{ Lang::get('core.m_pagecms')}}</span></a></li> 
            <li @if(Request::segment(2) == 'posts') class="active" @endif><a href="{{ url('core/posts')}}"><i class="fa  fa-file-text-o "></i> <span>Post Management</span></a></li>   
             <li @if(Request::segment(2) == 'logs') class="active" @endif><a href="{{ url('core/logs')}}"><i class="fa fa-circle-o "></i> <span>{{ Lang::get('core.m_logs') }} </span></a></li>
           </ul>

        </li>

         
         @endif      
    </ul>   


        <!-- /.control-sidebar-menu -->
     
    </section>
    <!-- /.sidebar -->
  </aside>